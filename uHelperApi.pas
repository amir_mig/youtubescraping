unit uHelperApi;

interface

uses
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.StrUtils,
  System.Variants,
  System.Classes,
  System.IniFiles,
  System.JSON,
  System.NetEncoding,
  MSXml,
  REST.Client,
  REST.Types,
  uModel, uApiServices;

function ApiYoutube(resource: string; params : TRESTRequestParameterList): TJSONValue;
function ApiCommentThreads(VideoId: string; PageResult: integer): TComments;

function ApiLanguage(sText : string; videoId : string) : TLanguages;
function JsonToVideo(jValue : TJSONValue ;  CustomerId: string; Keyword: string) : TVideo;
function JsonToVideos(jObject : TJSONObject ; CustomerId: string; Keyword: string) : TVideos;
//function JsonToVideoSearch(jValue : TJSONValue ;  CustomerId: string; Keyword: string) : TVideo;
function JsonToComment(json : TJSONValue) : TComment;
function JsonToComments(json : TJSONValue) : TComments;

const

sYoutubeBaseURL = 'https://www.googleapis.com/youtube/v3';
sLanguageWS = 'https://ws.detectlanguage.com/0.2/detect?q=' ;

implementation

function ApiYoutube(resource: string; params : TRESTRequestParameterList): TJSONValue;
var
  RESTRequest : TRESTRequest;
  RESTClient : TRESTCLient;
  RESTResponse : TRESTResponse;
  jValue : TJSONValue;

begin

  RESTClient := TRESTCLient.Create(nil);
  RESTRequest := TRESTRequest.Create(nil);
  RESTResponse := TRESTResponse.Create(nil);
  try
  RESTClient.BaseURL := sYoutubeBaseURL;
  RESTRequest.Resource := resource;
  RESTRequest.Client := RESTClient;
  RESTRequest.Response := RESTResponse;
  RESTRequest.Params := params;
  RESTRequest.Execute;

//  jValue := RESTResponse.JSONValue;
  Result :=  RESTResponse.JSONValue;
  finally
      RESTClient.Free;
      RESTRequest.Free;
      RESTResponse.Free;
  end;
end;

function JsonToVideo(jValue : TJSONValue ;  CustomerId: string; Keyword: string) : TVideo;
var
  video       : TVideo;
  language    : TLanguage;
  languages   : TLanguages;
  jSnippet    : TJSONObject;
  jStats      : TJSONValue;
  jDetails    : TJSONObject;
  jTags       : TJSONArray;
  jItem       : TJSONObject;
  sLikes : string;
  sDisLikes : string;
  sType       : string;
  cekTags     : boolean;
  Comments    : string;
  cekComments : boolean;
  cekLike : boolean;
  cekDisLike : boolean;
  sCustomerId : string;
//  jValue : TJSONValue;
  sBatchId : string;

begin
  sBatchId:=formatdatetime('yyyymmddhhmmss',Now);
//  jValue := JSON as TJSONValue;
  jSnippet:=jValue.GetValue<TJSONObject>('snippet');
  jStats:=jValue.GetValue<TJSONObject>('statistics');
  jDetails:=jValue.GetValue<TJSONObject>('contentDetails');
  cekTags:=jSnippet.TryGetValue<TJSONArray>('tags',jTags);
  cekLike := jStats.TryGetValue<string>('likeCount',sLikes);
  cekDisLike := jStats.TryGetValue<string>('dislikeCount',sDisLikes);
  cekComments:=jStats.TryGetValue<string>('commentCount',Comments);
  if jSnippet.TryGetValue<TJSONArray>('tags',jTags) then video.Tags:=jTags.ToString;
  if cekLike then video.Likes:= sLikes;
  if cekDisLike then video.DisLikes:= sDisLikes;
  if cekComments then video.Comments:=Comments;
  video.videoId:=jValue.GetValue<string>('id');
  video.CategoryId:=jSnippet.GetValue<string>('categoryId');
  video.PublishedDate:=jSnippet.GetValue<string>('publishedAt');
  video.ChannelId:=jSnippet.GetValue<string>('channelId');
  video.Title:=jSnippet.GetValue<string>('title');
  video.Description:=jSnippet.GetValue<string>('description');
  video.Duration:=jDetails.GetValue<string>('duration');
  video.Views:=jStats.GetValue<string>('viewCount');
  video.CustomerId:=CustomerId;
  video.Favorites:=jStats.GetValue<string>('favoriteCount');;
  video.Keyword:= Keyword;
  video.BatchId:=sBatchId;
  //video.languages:=GetLanguage(video.Description,video.videoId);
  Result:=video;

end;


function JsonToVideoSearch(jValue : TJSONValue ;  CustomerId: string; Keyword: string) : TVideo;
var
  video       : TVideo;
  jID         : TJSONObject;
  jSnippet    : TJSONObject;
  sBatchId : string;

begin
  sBatchId:=formatdatetime('yyyymmddhhmmss',Now);
//  jValue := JSON as TJSONValue;;
    jID := jValue.GetValue<TJSONObject>('id');
    jSnippet := jValue.GetValue<TJSONObject>('snippet');
    video.videoId := jID.GetValue<string>('videoId');
    video.ChannelId := jSnippet.GetValue<string>('channelId');
    video.PublishedDate := jSnippet.GetValue<string>('publishedAt');
    video.Title := jSnippet.GetValue<string>('title');
    video.Description := jSnippet.GetValue<string>('description');
  //video.languages:=GetLanguage(video.Description,video.videoId);
  Result:=video;

end;

function ApiLanguage(sText : string; videoId : string) : TLanguages;
var
  XMLHTTPRequest : IXMLHTTPRequest;
  oResponse      : TJSONValue;
  sResponse      : string;
  language       : TJSONValue;
  theLanguage    : string;
  theConfidence  : string;
  sReliable      : string;
  lLanguages     : TLanguages;
  jLanguages     : TJSONArray;
  I              : integer;const
sLanguageWS = 'https://ws.detectlanguage.com/0.2/detect?q=' ;
begin
  I:=0;
  XMLHTTPRequest:=CoXMLHTTP.Create;
  try
    XMLHTTPRequest.Open('GET',sLanguageWS +
    '"' + TNetEncoding.URL.Encode(sText) + '"',
      False,
      EmptyParam,
      EmptyParam);
    XMLHTTPRequest.setRequestHeader('Authorization','Bearer 36567e356b28eb56d43b62af6efe1697');
    XMLHTTPRequest.send('');
    sResponse:=XMLHTTPRequest.responseText;

    oResponse:=TJSONObject.ParseJSONValue(sResponse);
    jLanguages:=oResponse.GetValue<TJSONArray>('data.detections');

    if languages.Count > 0 then
    begin
      for language in jLanguages do
      begin
        SetLength(lLanguages,jLanguages.Count);
        lLanguages[I].videoId:=videoId;
        lLanguages[I].code:=language.GetValue<string>('language');
        lLanguages[I].Confidence:=language.GetValue<string>('confidence');
        if language.GetValue<string>('isReliable') = 'true' then lLanguages[I].reliable:='1'
        else lLanguages[I].reliable:='0';
        inc(I);
      end;

    end;
    result:=lLanguages;
  finally

  end;
end;

function ApiCommentThreads(VideoId: string; PageResult: integer): TComments;
var
  params : TRESTRequestParameterList;
  jValue : TJSONValue;
  comments : TComments;
  sString: string;
begin
params := TRESTRequestParameterList.Create(nil);
try
  params.AddItem('part','snippet');
  params.AddItem('key',GetApiKey());
  params.AddItem('videoId',VideoId);
  params.AddItem('maxResults','100');
  jValue := ApiYoutube('commentThreads',params);
  comments := JsonToComments(jValue);
//  Result := comment
finally
  params.Free;
end;


end;


function JsonToComment(json : TJSONValue) : TComment;
var
  comment : TComment;
  jsonValue : TJSONValue;
  jSnippet: TJSONObject;
  jStats: TJSONObject;
  sId : string;
begin
   jsonValue := json as TJSONValue;
    sId := jsonValue.GetValue<string>('id');
    jSnippet := jsonValue.GetValue<TJSONObject>('snippet');

    comment.AuthorDisplayName :=  jSnippet.GetValue<string>('authorDisplayName') ;
    comment.AuthorProfileImageUrl :=  jSnippet.GetValue<string>('authorProfileImageUrl') ;
    comment.AuthorChannelUrl :=  jSnippet.GetValue<string>('authorChannelUrl') ;
//    comment.ChannelId :=  jSnippet.GetValue<string>('channelId') ;
    comment.VideoId :=  jSnippet.GetValue<string>('videoId') ;
    comment.TextDisplay :=  jSnippet.GetValue<string>('textDisplay') ;
    comment.TextOriginal :=  jSnippet.GetValue<string>('textOriginal') ;
//    comment.ParentId :=  jSnippet.GetValue<string>('parentId') ;
    comment.ViewerRating :=  jSnippet.GetValue<string>('viewerRating') ;
    comment.LikeCount :=  jSnippet.GetValue<string>('likeCount') ;
//    comment.ModerationStatus :=  jSnippet.GetValue<string>('moderationStatus') ;
    comment.PublishedAt :=  jSnippet.GetValue<string>('publishedAt') ;
    comment.UpdatedAt :=  jSnippet.GetValue<string>('updatedAt') ;

    result := comment;
end;

function JsonToComments(json : TJSONValue) : TComments;
var
    // jObject: TJSONObject;
  jSnippet   : TJSONObject;
  jID        : TJSONObject;
  jItem      : TJSONArray;
  jpageInfo : TJSONValue;
  jItemValue : TJSONValue;
  jValue : TJSONValue;
  rec        : integer;
  sBatchId   : string;
  comment      : TComment;
  comments     : TComments;
begin


  jItem:=json.GetValue<TJSONArray>('items');
  SetLength(comments,jItem.Count);
  sBatchId:=formatdatetime('yyyymmddhhmmss',Now);
  for jItemValue in jItem do
  begin
    jID:=jItemValue.GetValue<TJSONObject>('id');
    jSnippet:=jItemValue.GetValue<TJSONObject>('snippet');
    comment:=JsonToComment(jItemValue);
    comments[rec]:=comment;
    inc(rec);
  end;
  result:=comments;
end;

function JsonToVideos(jObject : TJSONObject ; CustomerId: string; Keyword: string) : TVideos;
var

  jSnippet   : TJSONObject;
  jID        : TJSONObject;
  jItem      : TJSONArray;
  jpageInfo : TJSONValue;
  jItemValue : TJSONValue;
  rec        : integer;
  sBatchId   : string;
  video      : TVideo;
  videos     : TVideos;
begin

  jItem:=jObject.GetValue<TJSONArray>('items');
  SetLength(videos,jItem.Count);
  sBatchId:=formatdatetime('yyyymmddhhmmss',Now);
  rec :=0;
  for jItemValue in jItem do
  begin
    jID:=jItemValue.GetValue<TJSONObject>('id');
    jSnippet:=jItemValue.GetValue<TJSONObject>('snippet');
    video:=JsonTovideoSearch(jItemValue,CustomerId,Keyword);
    videos[rec]:=video;
    inc(rec);
  end;
  result:=videos;
end;



end.
