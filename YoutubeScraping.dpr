program YoutubeScraping;

uses
  Vcl.Forms,
  Vcl.Themes,
  Vcl.Styles,
  uMenu in 'uMenu.pas' {GridForm};

{$R *.res}

begin
  Application.Initialize;
  Application.UseMetropolisUI;
  Application.MainFormOnTaskbar := True;
  TStyleManager.TrySetStyle('Sapphire Kamri');
  Application.Title := 'Metropolis UI Application';

  Application.CreateForm(TGridForm, GridForm);
  Application.Run;
end.
