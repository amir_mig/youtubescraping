unit uScrappingHistory;

interface

uses
  Winapi.Windows,Winapi.Messages,System.SysUtils,System.Variants,System.Classes,Vcl.Graphics,
  Vcl.Controls,Vcl.Forms,Vcl.Dialogs,Vcl.ExtCtrls,Vcl.StdCtrls,
  Vcl.Imaging.pngimage,Vcl.Imaging.jpeg,System.Actions,Vcl.ActnList,
  Vcl.Styles,Vcl.Themes,Vcl.Touch.GestureMgr,Data.DB,Vcl.Grids,Vcl.DBGrids,uUtils,uSQL,Data.Win.ADODB;

type
  TScrappingHistoryForm = class(TForm)
    Panel1 : TPanel;
    TitleLabel : TLabel;
    Image1 : TImage;
    ScrollBox1 : TScrollBox;
    AppBar : TPanel;
    GestureManager1 : TGestureManager;
    ActionList1 : TActionList;
    Action1 : TAction;
    PanelController : TPanel;
    GroupBox1 : TGroupBox;
    ControllerGroupBox : TGroupBox;
    PanelDbGrid : TPanel;
    DBGrid1 : TDBGrid;
    CmbCustomer : TComboBox;
    LblCustomerCMB : TLabel;
    CmbKeywords : TComboBox;
    LblKeywords : TLabel;
    CmbChannel : TComboBox;
    LblChannel : TLabel;
    BtnSearch : TButton;
    DetailsPanel : TPanel;
    GroupBoxComment : TGroupBox;
    DBGridComment : TDBGrid;
    LblSearch : TLabel;
    EditSearch : TEdit;
    procedure BackToMainForm(Sender : TObject);
    procedure FormShow(Sender : TObject);
    procedure FormCreate(Sender : TObject);
    procedure Action1Execute(Sender : TObject);
    procedure FormResize(Sender : TObject);
    procedure FormKeyDown(Sender : TObject; var Key : Word; Shift : TShiftState);
    procedure FormGesture(Sender : TObject; const EventInfo : TGestureEventInfo;
      var Handled : Boolean);
    procedure FormCloseQuery(Sender : TObject; var CanClose : Boolean);
    procedure PopulateCustomerGroupData();
    function GetCustomerList(lCustomer : TStringList) : integer;
    procedure CmbCustomerDrawItem(Control : TWinControl; Index : integer; Rect : TRect; State : TOwnerDrawState);
    procedure CmbCustomerChange(Sender : TObject);
    procedure GetKeywords(CustId : String; Sender : TObject);
    procedure GetChannelEachCustomer(CustId : String; Sender : TObject);
    procedure CmbChannelDrawItem(Control : TWinControl; Index : integer; Rect : TRect; State : TOwnerDrawState);
    procedure CmbChannelChange(Sender : TObject);
    procedure BtnSearchClick(Sender : TObject);
    procedure DBGrid1CellClick(Column : TColumn);
    procedure DBGridCommentDrawColumnCell(Sender : TObject; const Rect : TRect; DataCol : integer; Column : TColumn; State : TGridDrawState);
    procedure dbGrid1CellWidthSetting();
    procedure EditSearchKeyPress(Sender : TObject; var Key : Char);

    private
        { Private declarations }
      procedure AppBarResize;
      procedure AppBarShow(mode : integer);
    public
        { Public declarations }
  end;

var
  ScrappingHistoryForm : TScrappingHistoryForm= nil;
  oSQLCustomerGrupTable,oSQLSearchCustomerGroup,oSQLCommentList,oSQLSearchVideo : TADOQuery;
  oDataSourceCustomerGrupTable,oDataSourceSearchCustomerGroup,oDataSourceCommentList,oDataSourceSearchVideo : TDataSource;
  sActiionCrudMode : String;
  sCustomerId            : String;
  sChannelId             : String;
  bFreeMemory            : Boolean;
  bCommentListEmptyValue : Boolean;
  bClearMemory           : Boolean;
  bSpecificSearch        : Boolean;

implementation

{$R *.dfm}


uses uMenu;

procedure TScrappingHistoryForm.Action1Execute(Sender : TObject);
begin
  AppBarShow(-1);
end;

const
  AppBarHeight = 75;

procedure TScrappingHistoryForm.AppBarResize;
begin
  AppBar.SetBounds(0,AppBar.Parent.Height - AppBarHeight,
    AppBar.Parent.Width,AppBarHeight);
end;

procedure FreeMemory();
begin
  oSQLSearchCustomerGroup.Free;
  oDataSourceSearchCustomerGroup.Free;

end;

procedure TScrappingHistoryForm.AppBarShow(mode : integer);
begin
  if mode = -1 then // Toggle
      mode:=integer(not AppBar.Visible);

  if mode = 0 then
      AppBar.Visible:=False
  else
  begin
    AppBar.Visible:=True;
    AppBar.BringToFront;
  end;
end;

procedure TScrappingHistoryForm.FormCloseQuery(Sender : TObject; var CanClose : Boolean);
begin
  oSQLCustomerGrupTable.Free;
  oDataSourceCustomerGrupTable.Free
end;

procedure TScrappingHistoryForm.FormCreate(Sender : TObject);
var
  LStyle                  : TCustomStyleServices;
  MemoColor,MemoFontColor : TColor;
begin
    // Set background color for memos to the color of the form, from the active style.
  LStyle:=TStyleManager.ActiveStyle;
  MemoColor:=LStyle.GetStyleColor(scGenericBackground);
  MemoFontColor:=LStyle.GetStyleFontColor(sfButtonTextNormal);

end;

procedure TScrappingHistoryForm.FormGesture(Sender : TObject;
  const EventInfo : TGestureEventInfo; var Handled : Boolean);
begin
  AppBarShow(0);
end;

procedure TScrappingHistoryForm.FormKeyDown(Sender : TObject; var Key : Word;
  Shift : TShiftState);
begin
  if Key = VK_ESCAPE then
      AppBarShow(-1)
  else
      AppBarShow(0);
end;

procedure TScrappingHistoryForm.FormResize(Sender : TObject);
begin
  AppBarResize;
end;

procedure TScrappingHistoryForm.PopulateCustomerGroupData();
var
  GroupElements : TStringList;
  memoStr       : String;
begin
  oSQLCustomerGrupTable:=SQLOpen('YouTubeScraping','SELECT * FROM CustomerGroup order by id asc');
  oSQLCustomerGrupTable.Open;
  oDataSourceCustomerGrupTable:=TDataSource.Create(ScrappingHistoryForm);
  try

    oDataSourceCustomerGrupTable.DataSet:=oSQLCustomerGrupTable;
    DBGrid1.DataSource:=oDataSourceCustomerGrupTable;

  except
    on e : exception do
    begin
      oSQLCustomerGrupTable.Free;
      oDataSourceCustomerGrupTable.Free;
    end;
  end;
end;

procedure TScrappingHistoryForm.FormShow(Sender : TObject);
var
  lCustomer : TStringList;
  lChannel  : TStringList;
  sSQL      : string;
begin
    // DBGrid1.OnDblClick:=nil;

  bClearMemory:=False;
  bCommentListEmptyValue:=False;
  sChannelId:='';
  CmbCustomer.Clear;
  CmbKeywords.Text:='';
  CmbKeywords.Clear;
  CmbChannel.Clear;
  EditSearch.Text:='';
  sCustomerId:='';
  lCustomer:=TStringList.Create;
  try
    GetCustomerList(lCustomer);
    CmbCustomer.Items:=lCustomer;
  finally
    lCustomer.Free;
  end;

  sSQL:='select ROW_NUMBER() OVER(ORDER BY id DESC) No,Keyword,Title,Cast(Substring(Descriptions,1,100) as varchar(500)) Descriptions,Cast(Substring(Tags,1,100) as varchar(500)) Tags,Likes ' +
    ',Dislikes,Views,Comments,Favorites,Duration ' +
    ' ,ChannelId,VideoID,CategoryID From Videos';
  oSQLSearchCustomerGroup:=SQLOpen('YouTubeScraping',sSQL);
  oSQLSearchCustomerGroup.Open;
  oDataSourceSearchCustomerGroup:=TDataSource.Create(ScrappingHistoryForm);

  try

    oDataSourceSearchCustomerGroup.DataSet:=oSQLSearchCustomerGroup;
    DBGrid1.DataSource:=oDataSourceSearchCustomerGroup;
    bFreeMemory:=True;
  except
    on e : exception do
    begin
      oSQLSearchCustomerGroup.Free;
    end;
  end;
  bFreeMemory:=True;
  dbGrid1CellWidthSetting();
end;

procedure TScrappingHistoryForm.BackToMainForm(Sender : TObject);
begin
  if (bFreeMemory = True) then
  begin
    oSQLSearchCustomerGroup.Free;
    oDataSourceSearchCustomerGroup.Free;
  end;

  if (bCommentListEmptyValue = True) then
  begin
    oSQLCommentList.Free;
    oDataSourceCommentList.Free;

  end;

  if (bClearMemory = True) then
  begin
    oSQLSearchVideo.Free;
    oDataSourceSearchVideo.Free;
  end;
    // EditSearch.Text:='';
  Hide;
  GridForm.BringToFront;
end;

procedure TScrappingHistoryForm.BtnSearchClick(Sender : TObject);
var
  sSQL : string;
begin
  if (bFreeMemory = True) then
  begin
    oSQLSearchCustomerGroup.Free;
    oDataSourceSearchCustomerGroup.Free;
  end;
  sSQL:='select ROW_NUMBER() OVER(ORDER BY id DESC) No,Keyword,Title,Cast(Substring(Descriptions,1,100) as varchar(500)) Descriptions,Cast(Substring(Tags,1,100) as varchar(500)) Tags,Likes ' +
    ',Dislikes,Views,Comments,Favorites,Duration ' +
    ' ,ChannelId,VideoID,CategoryID From Videos WHERE' +
    ' CustomerId LIKE ''%'+sCustomerId+'%'''+
    ' AND Keyword LIKE ''%'+CmbKeywords.Text+'%'''+
    ' AND ChannelId LIKE ''%'+sChannelId+'%''';

  oSQLSearchCustomerGroup:=SQLOpen('YouTubeScraping',sSQL);
  oSQLSearchCustomerGroup.Open;
  oDataSourceSearchCustomerGroup:=TDataSource.Create(ScrappingHistoryForm);

  try

    oDataSourceSearchCustomerGroup.DataSet:=oSQLSearchCustomerGroup;
    DBGrid1.DataSource:=oDataSourceSearchCustomerGroup;

  except
    on e : exception do
    begin
      oSQLSearchCustomerGroup.Free;
    end;
  end;
  bFreeMemory:=True;
  bSpecificSearch:=True;
  dbGrid1CellWidthSetting();

end;

procedure TScrappingHistoryForm.CmbChannelChange(Sender : TObject);
begin
  sChannelId:=CmbChannel.Items.ValueFromIndex[CmbChannel.ItemIndex];
end;

procedure TScrappingHistoryForm.CmbChannelDrawItem(Control : TWinControl; Index : integer; Rect : TRect; State : TOwnerDrawState);
begin
  CmbChannel.Canvas.TextRect(Rect,Rect.Left,Rect.Top,CmbChannel.Items.Names[Index]);
end;

procedure TScrappingHistoryForm.CmbCustomerChange(Sender : TObject);
begin
  sCustomerId:=CmbCustomer.Items.ValueFromIndex[CmbCustomer.ItemIndex];
  GetKeywords(sCustomerId,Sender);
  GetChannelEachCustomer(sCustomerId,Sender);
  sChannelId:='';
end;

procedure TScrappingHistoryForm.CmbCustomerDrawItem(Control : TWinControl; Index : integer; Rect : TRect; State : TOwnerDrawState);
begin
  CmbCustomer.Canvas.TextRect(Rect,Rect.Left,Rect.Top,CmbCustomer.Items.Names[Index]);
end;

procedure TScrappingHistoryForm.DBGrid1CellClick(Column : TColumn);
var
  sVideoId : String;
  sSQL     : string;
begin
  if (bCommentListEmptyValue=True) then
  begin
    oSQLCommentList.Free;
    oDataSourceCommentList.Free;
  end;

  sVideoId:=DBGrid1.Fields[12].AsString;
  sSQL:='SELECT ROW_NUMBER() OVER(ORDER BY id DESC) No, CommentId,AuthorDisplayName,';
  sSQL:=sSQL + 'Cast(Substring(TextDisplay,1,100) as varchar(500)) TextDisplay, AuthorProfileImageUrl ';
  sSQL:=sSQL + 'FROM dbo.VideoComments WHERE VideoID = '''+sVideoId+'''';
  oSQLCommentList:=SQLOpen('YouTubeScraping',sSQL);
    // oSQLCommentList.Open;
  oDataSourceCommentList:=TDataSource.Create(ScrappingHistoryForm);

  try
    oDataSourceCommentList.DataSet:=oSQLCommentList;
    DBGridComment.DataSource:=oDataSourceCommentList;
    bCommentListEmptyValue:=True;
  except
    on e : exception do
    begin
      oSQLCommentList.Free;
      oDataSourceCommentList.Free;
    end;
  end;

  DBGridComment.Columns[0].Width:=30;
  DBGridComment.Columns[1].Width:=200;
  DBGridComment.Columns[2].Width:=200;
  DBGridComment.Columns[3].Width:=750;
  DBGridComment.Columns[4].Width:=750;
    // DBGridComment.Columns[5].Width:=100;
    // DBGridComment.Columns[6].Width:=600;
    //
    // DBGridComment.Columns[7].Width:=100;
    // DBGridComment.Columns[8].Width:=100;
    // DBGridComment.Columns[9].Width:=100;
end;

procedure TScrappingHistoryForm.dbGrid1CellWidthSetting();
begin
  DBGrid1.Columns[0].Width:=50;
  DBGrid1.Columns[1].Width:=80;
  DBGrid1.Columns[2].Width:=300;
  DBGrid1.Columns[3].Width:=600;
  DBGrid1.Columns[4].Width:=300;
  DBGrid1.Columns[5].Width:=50;
  DBGrid1.Columns[6].Width:=50;
  DBGrid1.Columns[7].Width:=50;
  DBGrid1.Columns[8].Width:=50;
  DBGrid1.Columns[9].Width:=50;
  DBGrid1.Columns[10].Width:=80;
  DBGrid1.Columns[11].Width:=80;
  DBGrid1.Columns[11].Width:=80;

end;

procedure TScrappingHistoryForm.DBGridCommentDrawColumnCell(Sender : TObject; const Rect : TRect; DataCol : integer; Column : TColumn; State : TGridDrawState);
var
  S : String;
begin
  if (Column.Field.FieldName = 'CommentText') then
  begin
      // DBGridComment.Canvas.FillRect(Rect);
      // DBGridComment.Canvas.TextRect(Rect,Rect.Left,Rect.Top,Column.Field.Value);
    S:=Column.Field.AsString;
    DBGridComment.Canvas.Pen.Color:=clWindow;
    DBGridComment.Canvas.Brush.Color:=clWindow;
    DBGridComment.Canvas.Rectangle(Rect);
    DBGridComment.Canvas.TextOut(Rect.Left,Rect.Top,S);
  end;
end;

procedure TScrappingHistoryForm.EditSearchKeyPress(Sender : TObject; var Key : Char);
var sSQL : string; sSearchWord : String;
begin
  if (Key=#13) then
  begin
    if (bClearMemory = True) then
    begin
      oSQLSearchVideo.Free;
      oDataSourceSearchVideo.Free;
    end;
    sSearchWord:=EditSearch.Text;

    if sSearchWord.Contains('''') then
    begin
      sSearchWord:=StringReplace(sSearchWord,'''','''''',[rfReplaceAll])
    end;

    if (bFreeMemory = True) then
    begin
      if (bSpecificSearch = True) then
      begin
        sSQL:='Select  ROW_NUMBER() OVER(ORDER BY id DESC) No,Keyword,Title,Cast(Substring(Descriptions,1,100) as varchar(500)) Descriptions,Cast(Substring(Tags,1,100) as varchar(500)) Tags,Likes ' +
          ',Dislikes,Views,Comments,Favorites,Duration ' +
          ' ,ChannelId,VideoID,CategoryID From Videos where' +
          ' CustomerId LIKE ''%' + sCustomerId + '%''' + ' AND Keyword LIKE ''%'
          + CmbKeywords.Text + '%''' + ' AND ChannelId LIKE ''%' + sChannelId +
          '%''' + ' AND (Id LIKE ''%' + sSearchWord + '%''' +
          ' or VideoID LIKE ''%' + sSearchWord + '%''' +
          ' or Title LIKE ''%' + sSearchWord + '%''' +
          ' or Descriptions LIKE ''%' + sSearchWord + '%''' +
          ' or Tags LIKE ''%' + sSearchWord + '%''' +
          ' or CategoryID LIKE ''%' + sSearchWord + '%'')';
        oSQLSearchVideo:=SQLOpen('YouTubeScraping',sSQL);
        oDataSourceSearchVideo:=TDataSource.Create(ScrappingHistoryForm);

        try

          oDataSourceSearchVideo.DataSet:=oSQLSearchVideo;
          DBGrid1.DataSource:=oDataSourceSearchVideo;
          bClearMemory:=True;
          if (EditSearch.Text = '') then
          begin
            DBGrid1.DataSource:=oDataSourceSearchCustomerGroup;
          end;

        except
          on e : exception do
          begin
            oSQLSearchVideo.Free;
          end;
        end;
        dbGrid1CellWidthSetting();
      end
      else
      begin
        sSQL:='Select  ROW_NUMBER() OVER(ORDER BY id DESC) No,Keyword,Title,Cast(Substring(Descriptions,1,100) as varchar(500)) Descriptions,Cast(Substring(Tags,1,100) as varchar(500)) Tags,Likes ' +
          ' ,Dislikes,Views,Comments,Favorites,Duration ' +
          ' ,ChannelId,VideoID,CategoryID From Videos ' +
          ' where Id LIKE ''%' + sSearchWord + '%''' +
          ' or VideoID LIKE ''%'+sSearchWord+'%''' +
          ' or Keyword LIKE ''%'+sSearchWord+'%''' +
          ' or Title LIKE ''%'+sSearchWord+'%''' +
          ' or Descriptions LIKE ''%'+sSearchWord+'%''' +
          ' or Tags LIKE ''%'+sSearchWord+'%''' +
          ' or CategoryID LIKE ''%'+sSearchWord+'%''';
        oSQLSearchVideo:=SQLOpen('YouTubeScraping',sSQL);
        oDataSourceSearchVideo:=TDataSource.Create(ScrappingHistoryForm);

        try

          oDataSourceSearchVideo.DataSet:=oSQLSearchVideo;
          DBGrid1.DataSource:=oDataSourceSearchVideo;
          bClearMemory:=True;
          if (EditSearch.Text = '') then
          begin
            DBGrid1.DataSource:=oDataSourceSearchCustomerGroup;
          end;

        except
          on e : exception do
          begin
            oSQLSearchVideo.Free;
          end;
        end;
        dbGrid1CellWidthSetting();
      end;

    end;
  end;

  if (Key=#08) then
  begin
    if (Length(EditSearch.Text)= 1) then
    begin
      DBGrid1.DataSource:=oDataSourceSearchCustomerGroup;
    end;
  end;
end;

function TScrappingHistoryForm.GetCustomerList(lCustomer : TStringList) : integer;
var
  oSQL : TADOQuery;

begin
  try
    oSQL:=SQLOpen('YouTubeScraping','SELECT customer.Id as CustId ,'+
        ' customer.Name as CustName ,'+
        ' custGroup.Name as custGrupName'+
        ' from [dbo].[Customers] as customer'+
        ' INNER JOIN [dbo].[CustomerGroup] as custGroup ON customer.GroupId = custGroup.Id');
    oSQL.Recordset.MoveFirst;
    while not oSQL.Eof do
    begin
      lCustomer.AddPair(' [ '+oSQL.Recordset.Fields[2].Value +' ]- '+oSQL.Recordset.Fields[1].Value,oSQL.Recordset.Fields[0].Value);
      oSQL.Recordset.MoveNext;
      if oSQL.Recordset.Eof then Exit;
    end;
    result:=oSQL.RecordCount;
  finally
    oSQL.Free;
  end;

end;

procedure TScrappingHistoryForm.GetKeywords(CustId : String; Sender : TObject);
var
  oSQL         : TADOQuery;
  I            : integer;
  sKeywords    : String;
  arrayKeyword : TArray<String>;

begin
  oSQL:=SQLOpen('YouTubeScraping','SELECT Keywords FROM Customers where Id = '+CustId+'');
  oSQL.Open;
  try
    CmbKeywords.Clear;

    sKeywords:=oSQL.FieldByName('Keywords').AsString;
    arrayKeyword:=StringReplace(sKeywords,' ','',[rfReplaceAll]).Split([',']);
    for I:=0 to Length(arrayKeyword) -1 do
    begin

      CmbKeywords.AddItem(arrayKeyword[I],Sender);
    end;
  finally
    oSQL.Free;
  end;
end;

procedure TScrappingHistoryForm.GetChannelEachCustomer(CustId : String; Sender : TObject);
var
  oSQL,oSQLChannel : TADOQuery;
  sChannel         : String;
  arrayChannel     : TArray<String>;
  I,j              : integer;
  lChannel         : TStringList;

begin
  oSQL:=SQLOpen('YouTubeScraping','SELECT ChannelID FROM CustomerChannel where CustomerId = '+CustId+'');
  oSQL.Open;
  try
    CmbChannel.Clear;
    sChannel:=oSQL.FieldByName('ChannelID').AsString;
    arrayChannel:=StringReplace(sChannel,' ','',[rfReplaceAll]).Split([',']);
    lChannel:=TStringList.Create;
    try
      for I:=0 to Length(arrayChannel) -1 do
      begin
        oSQLChannel:=SQLOpen('YouTubeScraping','SELECT ChannelTitle , ChannelID FROM Channels where Id = '+arrayChannel[I]+'');
        oSQLChannel.Open;
        try
          if (oSQLChannel.FieldByName('ChannelTitle').AsString <> '') then
          begin
            lChannel.AddPair(oSQLChannel.FieldByName('ChannelTitle').AsString,oSQLChannel.FieldByName('ChannelID').AsString);
            CmbChannel.Items:=lChannel;
          end;
        finally
          oSQLChannel.Free;
        end;
      end;
    finally
      lChannel.Free;
    end;

  finally
    oSQL.Free;
  end;
end;

end.
