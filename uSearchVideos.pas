unit uSearchVideos;

interface

uses
  Winapi.Windows,Winapi.Messages,
  System.SysUtils,
  System.StrUtils,
  System.Variants,
  System.Classes,Vcl.Graphics,
  Vcl.Controls,Vcl.Forms,Vcl.Dialogs,IPPeerClient,System.Rtti,
  System.Bindings.Outputs,Vcl.Bind.Editors,Data.Bind.EngExt,
  Vcl.Bind.DBEngExt,Vcl.StdCtrls,Vcl.ExtCtrls,Data.Bind.Components,
  System.JSON,uSQL,Data.DB,
  Data.Win.ADODB,
  REST.Client,
  REST.Types,
  Data.Bind.ObjectScope,
  Vcl.Grids,
  uMenu,
  Vcl.Imaging.pngimage,uModel,
  MSXml,
  System.NetEncoding,
  inifiles,Vcl.ComCtrls, uHelperAPI;

type

  TFormSearchAPI = class(TForm)
    RESTClient1 : TRESTClient;
    RESTResponse1 : TRESTResponse;
    EditParamsquery : TEdit;
    BindingsList1 : TBindingsList;
    Panel1 : TPanel;
    BtnSearch: TButton;
    Label1 : TLabel;
    REST_Youtube_Request : TRESTRequest;
    StringGrid1 : TStringGrid;
    Image11 : TImage;
    ButtonSave : TButton;
    RESTRequestVid : TRESTRequest;
    Label3 : TLabel;
    EditChannel : TEdit;
    Label4 : TLabel;
    CbCustomer : TComboBox;
    LabelCustomer : TLabel;
    CbChannel : TComboBox;
    cbKeywords : TComboBox;
    EditMaxResult : TEdit;
    Label2 : TLabel;
    ProgressBar1 : TProgressBar;
    BtnPrevious : TButton;
    BtnNext : TButton;
    procedure RadioButton1Click(Sender : TObject);
    procedure RadioButton2Click(Sender : TObject);
    procedure SearchYoutube(pageToken : string);
    function ParseJsonVideo(JSON : TJSONValue; BatchId : string) : TVideo;
    function SaveVideos(jObject : TJSONObject) : TVideos;
    procedure SaveVideo(video : TVideo);
    procedure Image11Click(Sender : TObject);
    procedure ButtonSaveClick(Sender : TObject);
    procedure PopulateVideoGrid(videos : TVideos);
    function GetVideoInfo(id : string) : TVideo;
    function ParseJsonComment(JSON : TJSONValue) : TComment;
    procedure PopulateChannelGrid(channels : TChannels);
    function GetVideoIds(lVideoIds : TStringList; sChannelId : string) : integer;
    function GetVideos(listId : TStringList) : TVideos;
    function GetApiKey() : string;
    function GetLanguage(sPostedText : string; videoId : string) : TLanguages;
    procedure SaveLanguage(language : TLanguage);
    procedure FormShow(Sender : TObject);
    function GetCustomerList(lCustomer : TStringList) : integer;
    function GetChannelList(lChannel : TStringList) : integer;
    procedure CbCustomerChange(Sender : TObject);
    function GetVideoList(sChannelId : string; sPageToken : string) : TVideos;
    procedure CbCustomerDrawItem(Control : TWinControl; Index : integer; Rect : TRect; State : TOwnerDrawState);
    procedure CbChannelDrawItem(Control : TWinControl; Index : integer; Rect : TRect; State : TOwnerDrawState);
    procedure CbChannelChange(Sender : TObject);
    procedure GetKeywords(CustId : String; Sender : TObject);
    procedure GetChannelEachCustomer(CustId : String; Sender : TObject);
    procedure BtnNextClick(Sender : TObject);
    procedure BtnPreviousClick(Sender : TObject);
    function GetVideoData(videos : TVideos) : TVideos;
    procedure BtnSearchClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    function GetCommentList(svideoId : string; sPageToken : string) : TComments;
    function JsonToComment(json : TJSONValue) : TComment;
    procedure SaveComment(comment : TComment);
    procedure SaveCommentsToDB(video: TVideo);

  end;

var
  FormSearchAPI  : TFormSearchAPI;
  strSourceAPI   : string;
  jsonValue      : TJSONValue;
  videosP        : TVideos;
  videosQ        : TVideos;
  languagesP     : TLanguages;
  channelsP      : TChannels;
  sKey           : string;
  searchType     : string;
  sPrevPageToken : string;
  sNextPageToken : string;
  sNextCommentToken : string;
  sCustomerId    : String;
  sChannelId     : String;

implementation

{$R *.dfm}


procedure TFormSearchAPI.SearchYoutube(pageToken : string);
var
  lVideoIds : TStringList;
  sChannel  : string;
begin
  if CbChannel.Text <> '' then sChannel:=sChannelId
  else sChannel:='';
  lVideoIds:=TStringList.Create;
  sNextCommentToken :='';
  try
      // GetVideoIds(lVideoIds,sChannel);
    videosP:=GetVideoList(sChannel,pageToken);
    PopulateVideoGrid(videosP);
  finally
    lVideoIds.Free;
  end;
end;

procedure TFormSearchAPI.PopulateVideoGrid(videos : TVideos);
var I : integer;
begin
  StringGrid1.Rows[0].CommaText:='No,VideoId,Title,Description,PublishedDate,ChannelId';
  StringGrid1.ColWidths[0]:=50;
  StringGrid1.ColWidths[1]:=100;
  StringGrid1.ColWidths[2]:=300;
  StringGrid1.ColWidths[3]:=800;
  StringGrid1.ColWidths[4]:=200;
  StringGrid1.ColWidths[5]:=200;
    // StringGrid1.ColWidths[6]:=100;
    // StringGrid1.ColWidths[7]:=100;
    // StringGrid1.ColWidths[8]:=100;
    // StringGrid1.ColWidths[9]:=100;
    // sBatchId:=formatdatetime('yyyymmddhhmmss',Now);
  StringGrid1.RowCount:=length(videos) + 1;
  for I:=0 to length(videos)-1 do
  begin
    StringGrid1.Rows[I+1][0]:=(I+1).ToString;
    StringGrid1.Rows[I+1][1]:=videos[I].videoId;
    StringGrid1.Rows[I+1][2]:=videos[I].Title;
    StringGrid1.Rows[I+1][3]:=videos[I].Description;
    StringGrid1.Rows[I+1][4]:=videos[I].PublishedDate;
    StringGrid1.Rows[I+1][5]:=videos[I].ChannelId;
      // StringGrid1.Rows[I+1][6]:=FloatToStrF(StrToInt(videos[I].Views),ffNumber,8,0);
      // StringGrid1.Rows[I+1][7]:=FloatToStrF(StrToInt(videos[I].Likes),ffNumber,8,0);
      // StringGrid1.Rows[I+1][8]:=FloatToStrF(StrToInt(videos[I].Dislikes),ffNumber,8,0);
      // StringGrid1.Rows[I+1][9]:=FloatToStrF(StrToInt(videos[I].Comments),ffNumber,8,0);

  end;
end;

procedure TFormSearchAPI.PopulateChannelGrid(channels : TChannels);
var I : integer;
begin
  StringGrid1.Rows[0].CommaText:='No,Id,Name,Description,JoinDate';
  StringGrid1.ColWidths[0]:=50;
  StringGrid1.ColWidths[1]:=200;
  StringGrid1.ColWidths[2]:=300;
  StringGrid1.ColWidths[3]:=800;
  StringGrid1.ColWidths[4]:=200;
    // sBatchId:=formatdatetime('yyyymmddhhmmss',Now);
  for I:=0 to length(channels)-1 do
  begin
    StringGrid1.RowCount:=StringGrid1.RowCount + 1;
    StringGrid1.Rows[I+1][0]:=(I+1).ToString;
    StringGrid1.Rows[I+1][1]:=channels[I].ChannelId;
    StringGrid1.Rows[I+1][2]:=channels[I].ChannelTitle;
    StringGrid1.Rows[I+1][3]:=channels[I].Description;
  end;
end;

procedure TFormSearchAPI.BtnNextClick(Sender : TObject);
begin
  SearchYoutube(sNextPageToken);
  ProgressBar1.Position:=0;
end;

procedure TFormSearchAPI.BtnPreviousClick(Sender : TObject);
begin
  SearchYoutube(sPrevPageToken);
  ProgressBar1.Position:=0;
end;

procedure TFormSearchAPI.BtnSearchClick(Sender: TObject);
begin
    SearchYoutube('');
  ProgressBar1.Position:=0;
end;


function TFormSearchAPI.GetApiKey() : string;
var
  IniFile : TIniFile;
  strKey  : string;
begin
  IniFile:=TIniFile.Create(ExtractFilePath(ParamStr(0)) + 'YoutubeScraping.ini');
  try
    result:=IniFile.ReadString('YoutubeConfig','key','default');
  finally
    IniFile.Free;
  end;
end;

procedure TFormSearchAPI.Button2Click(Sender: TObject);
begin
  //SaveCommentsToDB(EditVideoId.Text);
  ShowMessage('Save comment completed');
end;

procedure TFormSearchAPI.SaveCommentsToDB(video: Tvideo);

var I : integer;
  tot : integer;
  comments : TComments;
begin
 tot := Strtoint(video.Comments);
 comments := GetCommentList(video.videoID,'');
   for I:=0 to Length(comments)-1 do
  begin
    SaveComment(comments[I]);
  end;

end;



procedure TFormSearchAPI.ButtonSaveClick(Sender : TObject);
var I : integer;
  tot : integer;
begin
  videosQ:=GetVideoData(videosP);
  tot:=length(videosQ);
  for I:=0 to tot-1 do
  begin
    SaveVideo(videosQ[I]);
    SaveCommentsToDB(videosQ[I]);
    ProgressBar1.Position:=round(100*(I+1)/tot);
  end;
  ShowMessage('Save videos completed');
end;

procedure TFormSearchAPI.CbChannelChange(Sender : TObject);
begin
  sChannelId:=CbChannel.Items.ValueFromIndex[CbChannel.ItemIndex];
end;

procedure TFormSearchAPI.CbChannelDrawItem(Control : TWinControl; Index : integer; Rect : TRect; State : TOwnerDrawState);
begin
  CbChannel.Canvas.TextRect(Rect,Rect.Left,Rect.Top,CbChannel.Items.Names[Index]);
end;

procedure TFormSearchAPI.GetKeywords(CustId : String; Sender : TObject);
var
  oSQL         : TADOQuery;
  I            : integer;
  sKeywords    : String;
  arrayKeyword : TArray<String>;

begin
  oSQL:=SQLOpen('YouTubeScraping','SELECT Keywords FROM Customers where Id = '+CustId+'');
  oSQL.Open;
  try
    cbKeywords.Clear;

    sKeywords:=oSQL.FieldByName('Keywords').AsString;
    arrayKeyword:=StringReplace(sKeywords,' ','',[rfReplaceAll]).Split([',']);
    for I:=0 to length(arrayKeyword) -1 do
    begin

      cbKeywords.AddItem(arrayKeyword[I],Sender);
    end;
  finally
    oSQL.Free;
  end;
end;

procedure TFormSearchAPI.GetChannelEachCustomer(CustId : String; Sender : TObject);
var
  oSQL,oSQLChannel : TADOQuery;
  sChannel         : String;
  arrayChannel     : TArray<String>;
  I,j              : integer;
  lChannel         : TStringList;

begin
  oSQL:=SQLOpen('YouTubeScraping','SELECT ChannelID FROM CustomerChannel where CustomerId = '+CustId+'');
  oSQL.Open;
  try
    CbChannel.Clear;
    sChannel:=oSQL.FieldByName('ChannelID').AsString;
    arrayChannel:=StringReplace(sChannel,' ','',[rfReplaceAll]).Split([',']);

    lChannel:=TStringList.Create;
    try
      for I:=0 to length(arrayChannel) -1 do
      begin
        oSQLChannel:=SQLOpen('YouTubeScraping','SELECT ChannelTitle , ChannelID FROM Channels where Id = '+arrayChannel[I]+'');
        oSQLChannel.Open;
        try
          if (oSQLChannel.FieldByName('ChannelTitle').AsString <> '') then
          begin
            lChannel.AddPair(oSQLChannel.FieldByName('ChannelTitle').AsString,oSQLChannel.FieldByName('ChannelID').AsString);
            CbChannel.Items:=lChannel;
          end;

        finally
          oSQLChannel.Free;
        end;
      end;
    finally
      lChannel.Free;
    end;

  finally
    oSQL.Free;
  end;
end;

procedure TFormSearchAPI.CbCustomerChange(Sender : TObject);
begin
  sCustomerId:=CbCustomer.Items.ValueFromIndex[CbCustomer.ItemIndex];
  GetKeywords(sCustomerId,Sender);
  GetChannelEachCustomer(sCustomerId,Sender);
  ProgressBar1.Position:=0;
end;

procedure TFormSearchAPI.CbCustomerDrawItem(Control : TWinControl; Index : integer; Rect : TRect; State : TOwnerDrawState);
begin
  CbCustomer.Canvas.TextRect(Rect,Rect.Left,Rect.Top,CbCustomer.Items.Names[Index]);
end;

procedure TFormSearchAPI.FormShow(Sender : TObject);
var

  lCustomer : TStringList;
  lChannel  : TStringList;

begin
  CbChannel.Text:='ALL';

  lCustomer:=TStringList.Create;
  lChannel:=TStringList.Create;
  try
    GetCustomerList(lCustomer);
    GetChannelList(lChannel);
    CbCustomer.Items:=lCustomer;
    CbChannel.Items:=lChannel;
  finally
    lCustomer.Free;
    lChannel.Free;
  end;

end;

procedure TFormSearchAPI.Image11Click(Sender : TObject);
begin
  Self.close;
  CbCustomer.Text:='';
  CbChannel.Text:='';
  cbKeywords.Text:='';
  CbCustomer.Clear;
  cbKeywords.Clear;
  CbChannel.Clear;
end;

procedure TFormSearchAPI.RadioButton1Click(Sender : TObject);
begin
  strSourceAPI:='gplus';
end;

procedure TFormSearchAPI.RadioButton2Click(Sender : TObject);
begin
  strSourceAPI:='youtube';
end;

function TFormSearchAPI.ParseJsonVideo(JSON : TJSONValue; BatchId : string) : TVideo;
var
  video       : TVideo;
  language    : TLanguage;
  languages   : TLanguages;
  jSnippet    : TJSONObject;
  jStats      : TJSONValue;
  jDetails    : TJSONObject;
  jTags       : TJSONArray;
  jItem       : TJSONObject;
  sLikes      : string;
  sDisLikes   : string;
  sType       : string;
  cekTags     : boolean;
  Comments    : string;
  cekComments : boolean;
  cekLike     : boolean;
  cekDisLike  : boolean;
begin
  jSnippet:=JSON.GetValue<TJSONObject>('snippet');
  jStats:=JSON.GetValue<TJSONObject>('statistics');
  jDetails:=JSON.GetValue<TJSONObject>('contentDetails');
  cekTags:=jSnippet.TryGetValue<TJSONArray>('tags',jTags);
  cekLike:=jStats.TryGetValue<string>('likeCount',sLikes);
  cekDisLike:=jStats.TryGetValue<string>('dislikeCount',sDisLikes);
  cekComments:=jStats.TryGetValue<string>('commentCount',Comments);
  if jSnippet.TryGetValue<TJSONArray>('tags',jTags) then video.Tags:=jTags.ToString;
  if cekLike then video.Likes:=sLikes;
  if cekDisLike then video.DisLikes:=sDisLikes;
  if cekComments then video.Comments:=Comments;
  video.videoId:=JSON.GetValue<string>('id');
  video.CategoryId:=jSnippet.GetValue<string>('categoryId');
  video.PublishedDate:=jSnippet.GetValue<string>('publishedAt');
  video.ChannelId:=jSnippet.GetValue<string>('channelId');
  video.Title:=jSnippet.GetValue<string>('title');
  video.Description:=jSnippet.GetValue<string>('description');
  video.Duration:=jDetails.GetValue<string>('duration');
  video.Views:=jStats.GetValue<string>('viewCount');
  video.CustomerId:=sCustomerId;
  video.Favorites:=jStats.GetValue<string>('favoriteCount');;
  video.Keyword:=cbKeywords.Text;
  video.BatchId:=BatchId;
    // video.languages:=GetLanguage(video.Description,video.videoId);
  result:=video;
    // jStats := json as TJSONValue;
end;

function TFormSearchAPI.ParseJsonComment(JSON : TJSONValue) : TComment;
var
  comment  : TComment;
  jSnippet : TJSONObject;
  jID      : TJSONObject;
begin
  jID:=JSON.GetValue<TJSONObject>('id');
  jSnippet:=JSON.GetValue<TJSONObject>('snippet');

  result:=comment;
end;

function TFormSearchAPI.SaveVideos(jObject : TJSONObject) : TVideos;
var
    // jObject: TJSONObject;
  jSnippet   : TJSONObject;
  jID        : TJSONObject;
  jItem      : TJSONArray;
  jItemValue : TJSONValue;
  rec        : integer;
  sBatchId   : string;
  video      : TVideo;
  videos     : TVideos;
begin
    // jObject := jsonVal as TJSONObject;
  rec:=0;
  SetLength(videos,5);
  jItem:=jObject.GetValue('items') as TJSONArray;
  sBatchId:=formatdatetime('yyyymmddhhmmss',Now);
  for jItemValue in jItem do
  begin
    jID:=jItemValue.GetValue<TJSONObject>('id');
    jSnippet:=jItemValue.GetValue<TJSONObject>('snippet');
    video:=ParseJsonVideo(jItemValue,sBatchId);
    videos[rec]:=video;
    inc(rec);
  end;
  result:=videos;
end;

function TFormSearchAPI.GetVideos(listId : TStringList) : TVideos;
var
  rec      : integer;
  sVideoID : string;
  videos   : TVideos;
begin

  rec:=0;
  SetLength(videos,listId.Count);
  for sVideoID in listId do
  begin
    videos[rec]:=GetVideoInfo(sVideoID);;
    inc(rec);
  end;
  result:=videos;
end;

function TFormSearchAPI.GetVideoData(videos : TVideos) : TVideos;
var
  rec        : integer;
  sVideoID   : string;
  videoInfos : TVideos;
begin
  SetLength(videoInfos,length(videos));
  for rec:=0 to length(videos)-1 do
  begin
    videoInfos[rec]:=GetVideoInfo(videos[rec].videoId);;
  end;
  result:=videoInfos;
end;

function TFormSearchAPI.GetVideoIds(lVideoIds : TStringList; sChannelId : string) : integer;
var
    // jObject: TJSONObject;
  jSnippet   : TJSONObject;
  jID        : TJSONObject;
  jItem      : TJSONArray;
  jItemValue : TJSONValue;
  rec        : integer;
  sBatchId   : string;
  video      : TVideo;
  jValue     : TJSONValue;
  jObject    : TJSONObject;

begin

  REST_Youtube_Request.Params.AddItem('part','snippet');
  REST_Youtube_Request.Params.AddItem('q',cbKeywords.Text);
  REST_Youtube_Request.Params.AddItem('maxResults',EditMaxResult.Text);
  REST_Youtube_Request.Params.AddItem('type','video');
  if sChannelId <> '' then REST_Youtube_Request.Params.AddItem('channelId',sChannelId);
  REST_Youtube_Request.Params.AddItem('key',GetApiKey());
  RESTClient1.BaseURL:='https://www.googleapis.com/youtube/v3';
  REST_Youtube_Request.Resource:='search';

  REST_Youtube_Request.Execute;
  jValue:=RESTResponse1.jsonValue;
  jObject:=jValue as TJSONObject;
  rec:=0;
  jItem:=jObject.GetValue('items') as TJSONArray;
  for jItemValue in jItem do
  begin
    jID:=jItemValue.GetValue<TJSONObject>('id');
    lVideoIds.Add(jID.GetValue<string>('videoId'));
  end;
  result:=lVideoIds.Count;

end;

function TFormSearchAPI.GetVideoList(sChannelId : string; sPageToken : string) : TVideos;
var
    // jObject: TJSONObject;
  jSnippet   : TJSONObject;
  jID        : TJSONObject;
  jItem      : TJSONArray;
  jItemValue : TJSONValue;
  rec        : integer;
  sBatchId   : string;
  video      : TVideo;
  jValue     : TJSONValue;
  jObject    : TJSONObject;
  videos     : TVideos;
  bCheckPrev : boolean;
  bCheckNext : boolean;

begin

  REST_Youtube_Request.Params.AddItem('part','snippet');
  REST_Youtube_Request.Params.AddItem('q',cbKeywords.Text);
  REST_Youtube_Request.Params.AddItem('maxResults',EditMaxResult.Text);
  REST_Youtube_Request.Params.AddItem('type','video');
  if sChannelId <> '' then REST_Youtube_Request.Params.AddItem('channelId',sChannelId);
  if sPageToken <> '' then REST_Youtube_Request.Params.AddItem('pageToken',sPageToken);
  REST_Youtube_Request.Params.AddItem('key',GetApiKey());
  RESTClient1.BaseURL:='https://www.googleapis.com/youtube/v3';
  REST_Youtube_Request.Resource:='search';

  REST_Youtube_Request.Execute;
  jValue:=RESTResponse1.jsonValue;
  jObject:=jValue as TJSONObject;
  rec:=0;
  bCheckNext:=jObject.TryGetValue<string>('nextPageToken',sNextPageToken);
  bCheckPrev:=jObject.TryGetValue<string>('prevPageToken',sPrevPageToken);

  if bCheckNext then BtnNext.Enabled:=True
  else BtnNext.Enabled:=False;
  if bCheckPrev then BtnPrevious.Enabled:=True
  else BtnPrevious.Enabled:=False;
  jItem:=jObject.GetValue('items') as TJSONArray;
  SetLength(videos,strtoint(EditMaxResult.Text));
  for jItemValue in jItem do
  begin

    jID:=jItemValue.GetValue<TJSONObject>('id');
    jSnippet:=jItemValue.GetValue<TJSONObject>('snippet');
    videos[rec].videoId:=jID.GetValue<string>('videoId');
    videos[rec].ChannelId:=jSnippet.GetValue<string>('channelId');
    videos[rec].PublishedDate:=jSnippet.GetValue<string>('publishedAt');
    videos[rec].Title:=jSnippet.GetValue<string>('title');
    videos[rec].Description:=jSnippet.GetValue<string>('description');
    inc(rec);
  end;
  result:=videos;

end;

procedure TFormSearchAPI.SaveVideo(video : TVideo);
var
  oSQL      : TADOQuery;
  sQuery    : string;
  lLanguage : TLanguage;
begin

  try
    oSQL:=SQLOpen('YoutubeScraping','Select id from Videos where VideoID ='+QuotedStr(video.videoId));
    while not oSQL.Eof do Exit;

    sQuery:='Insert Into Videos (CategoryId,Keyword,Title,Descriptions,Tags,Likes,Dislikes,Views,Favorites,Comments,Duration,ChannelId,CustomerId,IsValid,PublishedDate,BatchId,VideoID) Values(';
    sQuery:=sQuery + QuotedStr(video.CategoryId) +',';
    sQuery:=sQuery + QuotedStr(video.Keyword) +',';
    sQuery:=sQuery + QuotedStr(video.Title) +',';
    sQuery:=sQuery + QuotedStr(video.Description) + ',';
    sQuery:=sQuery + QuotedStr(video.Tags) + ',';
    sQuery:=sQuery + QuotedStr(video.Likes) + ',';
    sQuery:=sQuery + QuotedStr(video.DisLikes) + ',';
    sQuery:=sQuery + QuotedStr(video.Views) + ',';
    sQuery:=sQuery + QuotedStr(video.Favorites) + ',';
    sQuery:=sQuery + QuotedStr(video.Comments) + ',';
    sQuery:=sQuery + QuotedStr(video.Duration) + ',';
    sQuery:=sQuery + QuotedStr(video.ChannelId) + ',';
    sQuery:=sQuery + QuotedStr(video.CustomerId) + ',';
    sQuery:=sQuery + '0,';
    sQuery:=sQuery + QuotedStr(video.PublishedDate) + ',';
    sQuery:=sQuery + QuotedStr(video.BatchId) + ',';
    sQuery:=sQuery + QuotedStr(video.videoId) +')';
      // oSQL:=TADOQuery.Create(nil);
    if video.videoId <> '' then SQLExec('YoutubeScraping',sQuery);
    if length(video.languages) > 0 then
      for lLanguage in video.languages do
      begin
        SaveLanguage(lLanguage);
      end;
  finally
    oSQL.Free;
  end;
end;

procedure TFormSearchAPI.SaveLanguage(language : TLanguage);
var

  sQuery : string;
begin
  sQuery:='Insert Into Languages (Code,Reliable, Confidence,VideoId) Values(';
  sQuery:=sQuery + QuotedStr(language.code) +',';
  sQuery:=sQuery + QuotedStr(language.reliable) +',';
  sQuery:=sQuery + QuotedStr(language.Confidence) +',';
  sQuery:=sQuery + QuotedStr(language.videoId) + ')';
    // sQuery := sQuery + QuotedStr(channel.JoinDate)+')' ;
    // oSQL:=TADOQuery.Create(nil);
  if language.code <> '' then SQLExec('YoutubeScraping',sQuery);

end;

function TFormSearchAPI.GetVideoInfo(id : string) : TVideo;
var
  videoInfo : TVideo;

  jValue  : TJSONValue;
  jObject : TJSONObject;
  jItem   : TJSONArray;
  BatchId : string;

begin
  RESTClient1.BaseURL:='https://www.googleapis.com/youtube/v3';
  RESTRequestVid.Resource:='videos';
  RESTRequestVid.Params.AddItem('part','snippet, statistics,contentDetails');
  RESTRequestVid.Params.AddItem('key',GetApiKey());
  RESTRequestVid.Params.AddItem('id',id);
  RESTRequestVid.Execute;

  jValue:=RESTResponse1.jsonValue;
  jObject:=jValue as TJSONObject;
  jItem:=jObject.GetValue('items') as TJSONArray;
  BatchId:=formatdatetime('yyyymmddhhmmss',Now);
  jValue:=jItem.Items[0];
  videoInfo:=ParseJsonVideo(jValue,BatchId);
  result:=videoInfo;
end;

function TFormSearchAPI.GetLanguage(sPostedText : string; videoId : string) : TLanguages;
var
  XMLHTTPRequest : IXMLHTTPRequest;
  oResponse      : TJSONValue;
  sResponse      : string;
  language       : TJSONValue;
  theLanguage    : string;
  theConfidence  : string;
  sReliable      : string;
  lLanguages     : TLanguages;
  jLanguages     : TJSONArray;
  I              : integer;

begin
  I:=0;
  XMLHTTPRequest:=CoXMLHTTP.Create;
    // lLanguage := TStringList.Create;
  try
    XMLHTTPRequest.Open('GET',
      'https://ws.detectlanguage.com/0.2/detect?q="' +
        TNetEncoding.URL.Encode(sPostedText) + '"',
      False,
      EmptyParam,
      EmptyParam);
    XMLHTTPRequest.setRequestHeader('Authorization','Bearer 36567e356b28eb56d43b62af6efe1697');
    XMLHTTPRequest.send('');
    sResponse:=XMLHTTPRequest.responseText;

    oResponse:=TJSONObject.ParseJSONValue(sResponse);
    jLanguages:=oResponse.GetValue<TJSONArray>('data.detections');

    if languages.Count > 0 then
    begin
      for language in jLanguages do
      begin
        SetLength(lLanguages,jLanguages.Count);
        lLanguages[I].videoId:=videoId;
        lLanguages[I].code:=language.GetValue<string>('language');
        lLanguages[I].Confidence:=language.GetValue<string>('confidence');
        if language.GetValue<string>('isReliable') = 'true' then lLanguages[I].reliable:='1'
        else lLanguages[I].reliable:='0';
        inc(I);
      end;

    end;
    result:=lLanguages;
  finally

  end;
end;

function TFormSearchAPI.GetCustomerList(lCustomer : TStringList) : integer;
var
  oSQL : TADOQuery;

begin
  try
    oSQL:=SQLOpen('YouTubeScraping','SELECT customer.Id as CustId ,'+
        ' customer.Name as CustName ,'+
        ' custGroup.Name as custGrupName'+
        ' from [dbo].[Customers] as customer'+
        ' INNER JOIN [dbo].[CustomerGroup] as custGroup ON customer.GroupId = custGroup.Id');
    oSQL.Recordset.MoveFirst;
    while not oSQL.Eof do
    begin
      lCustomer.AddPair(' [ '+oSQL.Recordset.Fields[2].Value +' ]- '+oSQL.Recordset.Fields[1].Value,oSQL.Recordset.Fields[0].Value);
      oSQL.Recordset.MoveNext;
      if oSQL.Recordset.Eof then Exit;
    end;
    result:=oSQL.RecordCount;
  finally
    oSQL.Free;
  end;

end;

function TFormSearchAPI.GetChannelList(lChannel : TStringList) : integer;
var
  oSQL : TADOQuery;

begin
  try
    oSQL:=SQLOpen('YoutubeScraping','Select Channelid, ChannelTitle From Channels');
    oSQL.Recordset.MoveFirst;
    while not oSQL.Eof do
    begin
      lChannel.AddPair(oSQL.Recordset.Fields[1].Value,oSQL.Recordset.Fields[0].Value);
      oSQL.Recordset.MoveNext;
      if oSQL.Recordset.Eof then Exit;
    end;
    result:=oSQL.RecordCount;
  finally
    oSQL.Free;
  end;

end;

function TFormSearchAPI.GetCommentList(svideoId : string; sPageToken : string) : TComments;
var
    // jObject: TJSONObject;
  jSnippet   : TJSONObject;
  sID        : string;
  jItem      : TJSONArray;
  jItemValue : TJSONValue;
  rec        : integer;
  sBatchId   : string;
  video      : TVideo;
  jValue     : TJSONValue;
  jObject    : TJSONObject;
    comment     : TComment;
  comments     : TComments;
  bCheckPrev : boolean;
  bCheckNext : boolean;
    jTopLevel: TJSONValue;

begin
  REST_Youtube_Request.params.Clear;
  REST_Youtube_Request.params.AddItem('part','snippet');
  REST_Youtube_Request.params.AddItem('key',GetApiKey());
  REST_Youtube_Request.params.AddItem('videoId',svideoId);
  REST_Youtube_Request.params.AddItem('maxResults','100');

  RESTClient1.BaseURL:='https://www.googleapis.com/youtube/v3';
  REST_Youtube_Request.Resource:='commentThreads';

  REST_Youtube_Request.Execute;
  jValue:=RESTResponse1.jsonValue;
  jObject:=jValue as TJSONObject;
  rec:=0;

  jItem:=jObject.GetValue('items') as TJSONArray;
  SetLength(comments,jItem.Count);
  for jItemValue in jItem do
  begin
    sId:=jItemValue.GetValue<string>('id');
    jSnippet:=jItemValue.GetValue<TJSONObject>('snippet');
    jTopLevel := jSnippet.Getvalue('topLevelComment') as TJSONValue ;
    comment:=JsonToComment(jTopLevel);
    comment.CommentId := sId;
    comments[rec]:=comment;
    inc(rec);
  end;
  result:=comments;

end;


function TFormSearchAPI.JsonToComment(json : TJSONValue) : TComment;
var
  comment : TComment;
  jsonValue : TJSONValue;
  jObject, jSnippet: TJSONObject;
  jStats: TJSONObject;
  jTopLevel: TJSONValue;
  sId : string;

begin
    jObject := json as TJSONObject;//jsonValue.GetValue<TJSONObject>('snippet');
    jSnippet:=json.GetValue<TJSONObject>('snippet');
    comment.AuthorDisplayName := jSnippet.GetValue<string>('authorDisplayName') ;
    comment.AuthorProfileImageUrl := jSnippet.GetValue<string>('authorProfileImageUrl') ;
    comment.AuthorChannelUrl := jSnippet.GetValue<string>('authorChannelUrl') ;
    comment.VideoId := jSnippet.GetValue<string>('videoId') ;
    comment.TextDisplay := jSnippet.GetValue<string>('textDisplay') ;
    comment.TextOriginal := jSnippet.GetValue<string>('textOriginal') ;
    comment.ViewerRating := jSnippet.GetValue<string>('viewerRating') ;
    comment.LikeCount := jSnippet.GetValue<string>('likeCount') ;
    comment.PublishedAt := jSnippet.GetValue<string>('publishedAt') ;
    comment.UpdatedAt := jSnippet.GetValue<string>('updatedAt') ;
    Result := comment;
end;

procedure TFormSearchAPI.SaveComment(comment : TComment);
var
  oSQL      : TADOQuery;
  sQuery    : string;
  lLanguage : TLanguage;
begin

  try
    oSQL:=SQLOpen('YoutubeScraping','Select id from VideoComments where commentID ='+QuotedStr(comment.CommentId));
    while not oSQL.Eof do Exit;

    sQuery:='INSERT INTO VideoComments (VideoID,AuthorDisplayName,AuthorProfileImageUrl,TextDisplay,CommentId,TextOriginal,PublishedAt,UpdatedAt) Values( ' ;
    sQuery:=sQuery + QuotedStr(comment.VideoId) +',';
    sQuery:=sQuery + QuotedStr(comment.AuthorDisplayName) +',';
     sQuery:=sQuery + QuotedStr(comment.AuthorProfileImageUrl) +',';
     sQuery:=sQuery + QuotedStr(comment.TextDisplay) +',';
     sQuery:=sQuery + QuotedStr(comment.CommentId) +',';
     sQuery:=sQuery + QuotedStr(comment.TextOriginal) +',';
     sQuery:=sQuery + QuotedStr(comment.PublishedAt) +',';
     sQuery:=sQuery + QuotedStr(comment.UpdatedAt) +')';
      // oSQL:=TADOQuery.Create(nil);
    if sQuery <> '' then SQLExec('YoutubeScraping',sQuery);

  finally
    oSQL.Free;
  end;
end;

end.
