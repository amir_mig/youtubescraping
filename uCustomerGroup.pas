unit uCustomerGroup;

interface

uses
  Winapi.Windows,Winapi.Messages,System.SysUtils,System.Variants,System.Classes,Vcl.Graphics,
  Vcl.Controls,Vcl.Forms,Vcl.Dialogs,Vcl.ExtCtrls,Vcl.StdCtrls,
  Vcl.Imaging.pngimage,Vcl.Imaging.jpeg,System.Actions,Vcl.ActnList,
  Vcl.Styles,Vcl.Themes,Vcl.Touch.GestureMgr,Data.DB,Vcl.Grids,Vcl.DBGrids,uUtils,uSQL,Data.Win.ADODB;

type
  TCustomerGroupForm = class(TForm)
    Panel1 : TPanel;
    TitleLabel : TLabel;
    Image1 : TImage;
    ScrollBox1 : TScrollBox;
    AppBar : TPanel;
    GestureManager1 : TGestureManager;
    ActionList1 : TActionList;
    Action1 : TAction;
    CloseButton : TImage;
    PanelController : TPanel;
    GroupBox1 : TGroupBox;
    LblDescription : TLabel;
    LblName : TLabel;
    EditDescription : TEdit;
    EditId : TEdit;
    EditName : TEdit;
    BtnSave : TButton;
    BtnCancel : TButton;
    ControllerGroupBox : TGroupBox;
    LblSearch : TLabel;
    BtnDelete : TButton;
    BtnAdd : TButton;
    BtnUpdate : TButton;
    EditSearch : TEdit;
    PanelDbGrid : TPanel;
    DBGrid1 : TDBGrid;
    Label1 : TLabel;
    procedure BackToMainForm(Sender : TObject);
    procedure FormShow(Sender : TObject);
    procedure FormCreate(Sender : TObject);
    procedure Action1Execute(Sender : TObject);
    procedure FormResize(Sender : TObject);
    procedure FormKeyDown(Sender : TObject; var Key : Word; Shift : TShiftState);
    procedure FormGesture(Sender : TObject; const EventInfo : TGestureEventInfo;
      var Handled : Boolean);
    procedure BtnSaveClick(Sender : TObject);
    procedure BtnCancelClick(Sender : TObject);
    procedure BtnAddClick(Sender : TObject);
    procedure BtnUpdateClick(Sender : TObject);
    procedure BtnDeleteClick(Sender : TObject);
    procedure FormCloseQuery(Sender : TObject; var CanClose : Boolean);
    procedure DBGrid1CellClick(Column : TColumn);
    procedure EditSearchChange(Sender : TObject);
    procedure PopulateCustomerGroupData();

    private
        { Private declarations }
      procedure AppBarResize;
      procedure AppBarShow(mode : integer);
    public
        { Public declarations }
  end;

var
  CustomerGroupForm                                   : TCustomerGroupForm = nil;
  oSQLCustomerGrupTable,oSQLSearchCustomerGroup,oSQL5 : TADOQuery;
  oDataSourceCustomerGrupTable,oDataSourceSearchCustomerGroup,oDataSource5 : TDataSource;
  sActiionCrudMode : String;

implementation

{$R *.dfm}


uses uMenu;

procedure TCustomerGroupForm.Action1Execute(Sender : TObject);
begin
  AppBarShow(-1);
end;

const
  AppBarHeight = 75;

procedure TCustomerGroupForm.AppBarResize;
begin
  AppBar.SetBounds(0,AppBar.Parent.Height - AppBarHeight,
    AppBar.Parent.Width,AppBarHeight);
end;

procedure FreeMemory();
begin
  oSQLCustomerGrupTable.Free;
  oSQLSearchCustomerGroup.Free;
  oSQL5.Free;
end;

procedure TCustomerGroupForm.AppBarShow(mode : integer);
begin
  if mode = -1 then // Toggle
      mode:=integer(not AppBar.Visible);

  if mode = 0 then
      AppBar.Visible:=False
  else
  begin
    AppBar.Visible:=True;
    AppBar.BringToFront;
  end;
end;

procedure TCustomerGroupForm.FormCloseQuery(Sender : TObject; var CanClose : Boolean);
begin
  oSQLCustomerGrupTable.Free;
  oDataSourceCustomerGrupTable.Free
end;

procedure TCustomerGroupForm.FormCreate(Sender : TObject);
var
  LStyle                  : TCustomStyleServices;
  MemoColor,MemoFontColor : TColor;
begin
    // Set background color for memos to the color of the form, from the active style.
  LStyle:=TStyleManager.ActiveStyle;
  MemoColor:=LStyle.GetStyleColor(scGenericBackground);
  MemoFontColor:=LStyle.GetStyleFontColor(sfButtonTextNormal);

end;

procedure TCustomerGroupForm.FormGesture(Sender : TObject;
  const EventInfo : TGestureEventInfo; var Handled : Boolean);
begin
  AppBarShow(0);
end;

procedure TCustomerGroupForm.FormKeyDown(Sender : TObject; var Key : Word;
  Shift : TShiftState);
begin
  if Key = VK_ESCAPE then
      AppBarShow(-1)
  else
      AppBarShow(0);
end;

procedure TCustomerGroupForm.FormResize(Sender : TObject);
begin
  AppBarResize;
end;

procedure TCustomerGroupForm.PopulateCustomerGroupData();
var
  GroupElements : TStringList;
  memoStr       : String;
begin
  oSQLCustomerGrupTable:=SQLOpen('YouTubeScraping','SELECT * FROM CustomerGroup order by id asc');
  oSQLCustomerGrupTable.Open;
  oDataSourceCustomerGrupTable:=TDataSource.Create(CustomerGroupForm);
  try

    oDataSourceCustomerGrupTable.DataSet:=oSQLCustomerGrupTable;
    DBGrid1.DataSource:=oDataSourceCustomerGrupTable;
    EditId.Text:=oSQLCustomerGrupTable.FieldByName('Id').AsString;
    EditName.Text:=oSQLCustomerGrupTable.FieldByName('Name').AsString;
    EditDescription.Text:=oSQLCustomerGrupTable.FieldByName('Description').AsString;
  except
    on e : exception do
    begin
      oSQLCustomerGrupTable.Free;
      oDataSourceCustomerGrupTable.Free;
    end;
  end;
end;

procedure TCustomerGroupForm.FormShow(Sender : TObject);
begin
  PopulateCustomerGroupData();
end;

procedure TCustomerGroupForm.BackToMainForm(Sender : TObject);
begin
  FreeMemory();
  Hide;
  GridForm.BringToFront;
end;

procedure TCustomerGroupForm.BtnAddClick(Sender : TObject);
begin
  BtnSave.Enabled:=True;
  BtnCancel.Enabled:=True;
  EditDescription.Enabled:=True;
  EditName.Enabled:=True;
  BtnAdd.Enabled:=False;
  BtnUpdate.Enabled:=False;
  BtnDelete.Enabled:=False;
  EditName.SetFocus;
  EditId.Text:='';
  EditName.Text:='';
  EditDescription.Text:='';
  DBGrid1.Enabled:=False;
  sActiionCrudMode:='add';

end;

procedure TCustomerGroupForm.BtnCancelClick(Sender : TObject);
begin
  BtnSave.Enabled:=False;
  BtnCancel.Enabled:=False;
  EditDescription.Enabled:=False;
  EditName.Enabled:=False;
  BtnAdd.Enabled:=True;
  BtnUpdate.Enabled:=True;
  BtnDelete.Enabled:=True;
  if (DBGrid1.Enabled = False) then
  begin
    DBGrid1.Enabled:=True;
  end;
  EditId.Text:=oSQLCustomerGrupTable.FieldByName('Id').AsString;
  EditName.Text:=oSQLCustomerGrupTable.FieldByName('Name').AsString;
  EditDescription.Text:=oSQLCustomerGrupTable.FieldByName('Description').AsString;
  sActiionCrudMode:='none';
  if (BtnSave.Caption = 'Delete') then
  begin
    BtnSave.Caption:='Save';
  end
  else if (BtnSave.Caption = 'Update') then
  begin
    BtnSave.Caption:='Save';
  end;
end;

procedure TCustomerGroupForm.BtnDeleteClick(Sender : TObject);
begin
  BtnSave.Caption:='Delete';
  sActiionCrudMode:='delete';
  BtnSave.Enabled:=True;
  BtnCancel.Enabled:=True;
  BtnAdd.Enabled:=False;
  BtnUpdate.Enabled:=False;
  BtnDelete.Enabled:=False;

end;

procedure TCustomerGroupForm.BtnSaveClick(Sender : TObject);
var
  oSQL         : TADOQuery;
  sName        : String;
  sDescription : String;
begin
  if (sActiionCrudMode = 'add') then
  begin
    if (EditName.Text = '') then
    begin
      application.messageBox('Name can not be empty !','Warning',MB_ICONWARNING);
      EditName.SetFocus;
    end
    else
    begin
      try
        oSQL:=SQLOpen('YouTubeScraping','Select * from CustomerGroup Where 1=2');
        oSQL.Insert;
        oSQL.FieldByName('Name').AsString:=EditName.Text;
        oSQL.FieldByName('Description').AsString:=EditDescription.Text;
        oSQL.Post;
        ShowMessage('Data Saved!');

        if (DBGrid1.Enabled = False) then
        begin
          DBGrid1.Enabled:=True;
        end;
        BtnSave.Enabled:=False;
        BtnCancel.Enabled:=False;
        BtnAdd.Enabled:=True;
        BtnUpdate.Enabled:=True;
        BtnDelete.Enabled:=True;
        FreeMemory();
        FormShow(Sender);

      finally
        oSQL.Free;

      end;
    end;

  end
  else
    if (sActiionCrudMode = 'delete') then
  begin
    if (messagedlg('are you sure want to delete this data ?',mtConfirmation,
        [mbYes,mbCancel],0) = mrYes) then
    begin
      SQLExec('YouTubeScraping','DELETE from CustomerGroup Where Id ='''+EditId.Text+'''');
      ShowMessage('Data has been deleted!');
      BtnSave.Enabled:=False;
      BtnCancel.Enabled:=False;
      BtnAdd.Enabled:=True;
      BtnUpdate.Enabled:=True;
      BtnDelete.Enabled:=True;
      BtnSave.Caption:='Save';
      FreeMemory();
      FormShow(Sender);
    end;
  end
  else
    if (sActiionCrudMode = 'update') then
  begin
    if (EditName.Text = '') then
    begin
      application.messageBox('Name can not be empty !','Warning',MB_ICONWARNING);
      EditName.SetFocus;
    end
    else
    begin
      sName:=EditName.Text;
      sDescription:=EditDescription.Text;
      if sName.Contains('''') then
      begin
        sName:=StringReplace(sName,'''','''''',[rfReplaceAll])
      end;

      if sDescription.Contains('''') then
      begin
        sDescription:=StringReplace(sDescription,'''','''''',[rfReplaceAll])
      end;

      SQLExec('YouTubeScraping','UPDATE CustomerGroup SET'
          +' Name= '''+sName+''' '+
          ',Description='''+sDescription+''' '+
          ' where Id='''+EditId.Text+''' ');

      application.messageBox('Data Updated!','Success',MB_OK);
      BtnSave.Enabled:=False;
      BtnCancel.Enabled:=False;
      BtnAdd.Enabled:=True;
      BtnUpdate.Enabled:=True;
      BtnDelete.Enabled:=True;
      EditDescription.Enabled:=False;
      EditName.Enabled:=False;
      EditId.Enabled:=False;
      BtnSave.Caption:='Save';
      FreeMemory();
      FormShow(Sender);
      if (DBGrid1.Enabled = False) then
      begin
        DBGrid1.Enabled:=True;
      end;
    end;
  end;
end;

procedure TCustomerGroupForm.BtnUpdateClick(Sender : TObject);
begin
  sActiionCrudMode:='update';
  BtnSave.Enabled:=True;
  BtnCancel.Enabled:=True;
  BtnAdd.Enabled:=False;
  BtnUpdate.Enabled:=False;
  BtnDelete.Enabled:=False;
  EditName.Enabled:=True;
  EditDescription.Enabled:=True;
  BtnSave.Caption:='Update';
end;

procedure TCustomerGroupForm.DBGrid1CellClick(Column : TColumn);
begin
  if (EditSearch.Text = '') then
  begin
    EditId.Text:=oSQLCustomerGrupTable.FieldByName('Id').AsString;
    EditName.Text:=oSQLCustomerGrupTable.FieldByName('Name').AsString;
    EditDescription.Text:=oSQLCustomerGrupTable.FieldByName('Description').AsString;
  end
  else
  begin
    DBGrid1.DataSource:=oDataSourceSearchCustomerGroup;
    EditId.Text:=oSQLSearchCustomerGroup.FieldByName('Id').AsString;
    EditName.Text:=oSQLSearchCustomerGroup.FieldByName('Name').AsString;
    EditDescription.Text:=oSQLSearchCustomerGroup.FieldByName('Description').AsString;
  end;
end;

procedure TCustomerGroupForm.EditSearchChange(Sender : TObject);
begin
  oSQLSearchCustomerGroup:=SQLOpen('YouTubeScraping','Select * from CustomerGroup where Name LIKE ''%'+EditSearch.Text+'%'''+
      'or Description LIKE ''%'+EditSearch.Text+'%''' +
      'or Id LIKE ''%'+EditSearch.Text+'%''');
  oSQLSearchCustomerGroup.Open;
  oDataSourceSearchCustomerGroup:=TDataSource.Create(CustomerGroupForm);

  try

    oDataSourceSearchCustomerGroup.DataSet:=oSQLSearchCustomerGroup;
    DBGrid1.DataSource:=oDataSourceSearchCustomerGroup;
    if (EditSearch.Text = '') then
    begin
      DBGrid1.DataSource:=oDataSourceCustomerGrupTable;
    end;

  except
    on e : exception do
    begin
      oSQLSearchCustomerGroup.Free;
    end;
  end;
end;

end.
