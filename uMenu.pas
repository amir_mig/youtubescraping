unit uMenu;

interface

uses
  Winapi.Windows,Winapi.Messages,System.SysUtils,System.Variants,System.Classes,Vcl.Graphics,
  Vcl.Controls,Vcl.Forms,Vcl.Dialogs,Vcl.ExtCtrls,Vcl.StdCtrls,
  Vcl.Imaging.jpeg,Vcl.Imaging.pngimage,Vcl.ImgList,System.Actions,
  Vcl.ActnList,Vcl.Touch.GestureMgr,System.ImageList;

type
  TGridForm = class(TForm)
    Panel1 : TPanel;
    LbTitle : TLabel;
    GroupPanel1 : TPanel;
    Label2 : TLabel;
    FlowPanel1 : TFlowPanel;
    GroupPanel1_1 : TPanel;
    Image1 : TImage;
    Panel5 : TPanel;
    Label3 : TLabel;
    Label4 : TLabel;
    GroupPanel2_1 : TPanel;
    Image4 : TImage;
    Panel2 : TPanel;
    GroupPanel3 : TPanel;
    FlowPanel3 : TFlowPanel;
    GroupPanel3_1 : TPanel;
    GroupPanel3_3 : TPanel;
    Panel8 : TPanel;
    Panel10 : TPanel;
    Image6 : TImage;
    Image8 : TImage;
    ScrollBox2 : TScrollBox;
    AppBar : TPanel;
    Label12 : TLabel;
    Label14 : TLabel;
    Label22 : TLabel;
    Panel7 : TPanel;
    GestureManager1 : TGestureManager;
    ActionList1 : TActionList;
    Action1 : TAction;
    Label10 : TLabel;
    Panel3 : TPanel;
    Image2 : TImage;
    Panel4 : TPanel;
    Label1 : TLabel;
    Label5 : TLabel;
    Image3 : TImage;
    Image5 : TImage;
    Image11 : TImage;
    GroupPanel4 : TPanel;
    Image7 : TImage;
    Panel9 : TPanel;
    Label6 : TLabel;
    Panel6 : TPanel;
    Label8 : TLabel;
    Image9 : TImage;
    Image10 : TImage;
    Panel11 : TPanel;
    Label7 : TLabel;
    Panel12 : TPanel;
    procedure ScrollBox2Resize(Sender : TObject);
    procedure Image11Click(Sender : TObject);
    procedure FormResize(Sender : TObject);
    procedure FormShow(Sender : TObject);
    procedure Action1Execute(Sender : TObject);
    procedure FormKeyDown(Sender : TObject; var Key : Word; Shift : TShiftState);
    procedure FormGesture(Sender : TObject; const EventInfo : TGestureEventInfo;
      var Handled : Boolean);
    procedure Image4Click(Sender : TObject);
    procedure Image6Click(Sender : TObject);
    procedure Image8Click(Sender : TObject);
    procedure Image1Click(Sender : TObject);
    procedure Image2Click(Sender : TObject);
    procedure Image7Click(Sender : TObject);
    procedure Image10Click(Sender : TObject);
    private
        { Private declarations }
      procedure AppBarResize;
      procedure AppBarShow(mode : integer);
    public
        { Public declarations }
      searchType    : string;
      SelectedGroup : String; // group string from
      procedure PickImageColor(img : TImage; AColor : TColor);
  end;

const GenericText = 'Sed ut perspiciatis unde omnis iste natus error ' +
    'sit voluptatem accusantium doloremque laudantium, totam rem aperiam, ' +
    'eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae ' +
    'vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas ' +
    'sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores ' +
    'eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, ' +
    'qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, ' +
    'sed quia non numquam eius modi tempora incidunt ut labore et dolore ' +
    'magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis ' +
    'nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut ' +
    'aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit ' +
    'qui in ea voluptate velit esse quam nihil molestiae consequatur, vel ' +
    'illum qui dolorem eum fugiat quo voluptas nulla pariatur?';

var
  GridForm : TGridForm;

implementation

{$R *.dfm}


uses uSearchChannels,uSearchVideos,uCustomerGroup,YoutubeConfiguration,uCustomer,uScrappingHistory,uChannelHistory;

const
  AppBarHeight = 75;

procedure TGridForm.AppBarResize;
begin
  AppBar.SetBounds(0,AppBar.Parent.Height - AppBarHeight,
    AppBar.Parent.Width,AppBarHeight);
end;

procedure TGridForm.AppBarShow(mode : integer);
begin
  if mode = -1 then // Toggle
      mode:=integer(not AppBar.Visible);
  if mode = 0 then
      AppBar.Visible:=False
  else
  begin
    AppBar.Visible:=True;
    AppBar.BringToFront;
  end;
end;

procedure TGridForm.Action1Execute(Sender : TObject);
begin
  AppBarShow(-1);
end;

procedure TGridForm.FormGesture(Sender : TObject;
  const EventInfo : TGestureEventInfo; var Handled : Boolean);
begin
  AppBarShow(0);
end;

procedure TGridForm.FormKeyDown(Sender : TObject; var Key : Word;
  Shift : TShiftState);
begin
  if Key = VK_ESCAPE then
      AppBarShow(-1)
  else
      AppBarShow(0);
end;

procedure TGridForm.FormResize(Sender : TObject);
begin
  AppBarResize;
end;

procedure TGridForm.FormShow(Sender : TObject);
begin
  AppBarShow(0);
end;

procedure TGridForm.Image10Click(Sender : TObject);
begin
  if not Assigned(ChannelHistoryForm) then
      ChannelHistoryForm:=TChannelHistoryForm.Create(Self);
  ChannelHistoryForm.Show;
  ChannelHistoryForm.BringToFront;
end;

procedure TGridForm.Image11Click(Sender : TObject);
begin
  Application.Terminate;
end;

procedure TGridForm.Image1Click(Sender : TObject);
begin
  if not Assigned(FormSearchAPI) then FormSearchAPI:=TFormSearchAPI.Create(Self);
  searchType:='video';
  FormSearchAPI.Show;
  FormSearchAPI.BringToFront;
end;

procedure TGridForm.Image2Click(Sender : TObject);
begin
  if not Assigned(FormSearchChannels) then FormSearchChannels:=TFormSearchChannels.Create(Self);
  FormSearchChannels.Show;
  FormSearchChannels.BringToFront;

end;

procedure TGridForm.Image4Click(Sender : TObject);
begin
  if not Assigned(CustomerGroupForm) then
      CustomerGroupForm:=TCustomerGroupForm.Create(Self);
  CustomerGroupForm.Show;
  CustomerGroupForm.BringToFront;
end;

procedure TGridForm.Image6Click(Sender : TObject);
begin
  if not Assigned(FormYoutubeConfig) then
      FormYoutubeConfig:=TFormYoutubeConfig.Create(Self);
  FormYoutubeConfig.Show;
  FormYoutubeConfig.BringToFront;
end;

procedure TGridForm.Image7Click(Sender : TObject);
begin
  if not Assigned(ScrappingHistoryForm) then
      ScrappingHistoryForm:=TScrappingHistoryForm.Create(Self);
  ScrappingHistoryForm.Show;
  ScrappingHistoryForm.BringToFront;
end;

procedure TGridForm.Image8Click(Sender : TObject);
begin
  if not Assigned(CustomerForm) then
      CustomerForm:=TCustomerForm.Create(Self);
  CustomerForm.Show;
  CustomerForm.BringToFront;
end;

procedure TGridForm.PickImageColor(img : TImage; AColor : TColor);
begin
  img.Canvas.Brush.Color:=AColor;
  img.Canvas.Brush.Style:=bsSolid;
  img.Canvas.FillRect(img.ClientRect);
  img.Canvas.Refresh;
end;

procedure TGridForm.ScrollBox2Resize(Sender : TObject);
begin
  GroupPanel1.Height:=TControl(Sender).ClientHeight-10;
  GroupPanel3.Height:=TControl(Sender).ClientHeight-10;

end;

end.
