unit YoutubeConfiguration;

interface

uses
  Winapi.Windows,Winapi.Messages,System.SysUtils,System.Variants,System.Classes,Vcl.Graphics,
  Vcl.Controls,Vcl.Forms,Vcl.Dialogs,Vcl.StdCtrls,Vcl.ExtCtrls,inifiles,uEncription;

type
  TFormYoutubeConfig = class(TForm)
    EditEmail : TEdit;
    Panel1 : TPanel;
    LblName : TLabel;
    LblKey : TLabel;
    BtnSaveConfig : TButton;
    MemoKey: TMemo;
    procedure BtnSaveConfigClick(Sender : TObject);
    procedure FormShow(Sender : TObject);
    private
        { Private declarations }
    public
        { Public declarations }
  end;

var
  FormYoutubeConfig : TFormYoutubeConfig;

implementation

{$R *.dfm}


procedure TFormYoutubeConfig.BtnSaveConfigClick(Sender : TObject);
var
  IniFile   : TIniFile;
  ValueList : TStringList;
  sOldEmail : String;
  sOldKey   : string;
begin
  IniFile:=TIniFile.Create(ExtractFilePath(ParamStr(0)) + 'YoutubeScraping.ini');
  if (EditEmail.Text <> '') and (MemoKey.Text <> '') then
  begin
    sOldEmail:=IniFile.ReadString('YoutubeConfig','email','default');
    sOldKey:=IniFile.ReadString('YoutubeConfig','key','default');

    if ((sOldEmail = EditEmail.Text) AND (sOldKey = MemoKey.Text)) then
    begin
      ShowMessage('Nothing Change!');
    end
    else
      if ((sOldEmail = EditEmail.Text) AND (sOldKey <> MemoKey.Text)) then
    begin
      IniFile.WriteString('YoutubeConfig','key',MemoKey.Text);
      IniFile.Free;
      ShowMessage('New key has been updated!');
      Self.Close;
    end
    else
      if ((sOldEmail <> EditEmail.Text) AND (sOldKey = MemoKey.Text)) then
    begin
      IniFile.WriteString('YoutubeConfig','email',EditEmail.Text);
      IniFile.Free;
      ShowMessage('New email has been Saved!');
      Self.Close;
    end
    else
      if ((sOldEmail <> EditEmail.Text) AND (sOldKey <> MemoKey.Text)) then
    begin
      IniFile.WriteString('YoutubeConfig','email',EditEmail.Text);
      IniFile.WriteString('YoutubeConfig','key',MemoKey.Text);
      IniFile.Free;
      ShowMessage('Configuration Saved!');
      Self.Close;
    end;

  end
  else
  begin
    ShowMessage('Field cannot be empty');
  end;

end;

procedure TFormYoutubeConfig.FormShow(Sender : TObject);
var
  IniFile   : TIniFile;
  ValueList : TStringList;
begin
  IniFile:=TIniFile.Create(ExtractFilePath(ParamStr(0)) + 'YoutubeScraping.ini');
  EditEmail.Text:=IniFile.ReadString('YoutubeConfig','email','default');
  MemoKey.Text:=IniFile.ReadString('YoutubeConfig','key','default');
end;

end.
