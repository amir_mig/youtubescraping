unit uCustomer;

interface

uses
  Winapi.Windows,Winapi.Messages,System.SysUtils,System.Variants,System.Classes,Vcl.Graphics,
  Vcl.Controls,Vcl.Forms,Vcl.Dialogs,Vcl.ExtCtrls,Vcl.StdCtrls,
  Vcl.Imaging.pngimage,Vcl.Imaging.jpeg,System.Actions,Vcl.ActnList,
  Vcl.Styles,Vcl.Themes,Vcl.Touch.GestureMgr,Data.DB,Vcl.Grids,Vcl.DBGrids,uUtils,uSQL,Data.Win.ADODB,System.StrUtils,Vcl.DBCtrls;

type
  TCustomerForm = class(TForm)
    Panel1 : TPanel;
    TitleLabel : TLabel;
    Image1 : TImage;
    ScrollBox1 : TScrollBox;
    AppBar : TPanel;
    GestureManager1 : TGestureManager;
    ActionList1 : TActionList;
    Action1 : TAction;
    CloseButton : TImage;
    PanelController : TPanel;
    GroupBox1 : TGroupBox;
    LblDescription : TLabel;
    LblId : TLabel;
    LblCustName : TLabel;
    EditDescription : TEdit;
    EditId : TEdit;
    EditName : TEdit;
    BtnSave : TButton;
    BtnCancel : TButton;
    ControllerGroupBox : TGroupBox;
    LblSearch : TLabel;
    BtnDelete : TButton;
    BtnAdd : TButton;
    BtnUpdate : TButton;
    EditSearch : TEdit;
    PanelDbGrid : TPanel;
    DBGrid1 : TDBGrid;
    MemoKeywords : TMemo;
    LblKeyword : TLabel;
    LblChannel : TLabel;
    LblCustomerGroup : TLabel;
    CmbCustomerGrup : TComboBox;
    DBGridChannel : TDBGrid;
    GroupBoxChannelList : TGroupBox;
    MemoChannelList : TMemo;
    StringGrid1 : TStringGrid;
    LblSearchChannel : TLabel;
    EditSearchChannel : TEdit;
    procedure BackToMainForm(Sender : TObject);
    procedure FormShow(Sender : TObject);
    procedure FormCreate(Sender : TObject);
    procedure Action1Execute(Sender : TObject);
    procedure FormResize(Sender : TObject);
    procedure FormKeyDown(Sender : TObject; var Key : Word; Shift : TShiftState);
    procedure FormGesture(Sender : TObject; const EventInfo : TGestureEventInfo;
      var Handled : Boolean);
    procedure BtnSaveClick(Sender : TObject);
    procedure BtnCancelClick(Sender : TObject);
    procedure BtnAddClick(Sender : TObject);
    procedure BtnUpdateClick(Sender : TObject);
    procedure BtnDeleteClick(Sender : TObject);
    procedure FormCloseQuery(Sender : TObject; var CanClose : Boolean);
    procedure DBGrid1CellClick(Column : TColumn);
    procedure EditSearchChange(Sender : TObject);
    procedure MemoKeywordsChange(Sender : TObject);
    procedure MemoKeywordsKeyPress(Sender : TObject; var Key : Char);
    procedure DBGridChannelCellClick(Column : TColumn);
    procedure StringGrid1MouseDown(Sender : TObject; Button : TMouseButton; Shift : TShiftState; X,Y : Integer);
    procedure StringGrid1MouseWheelDown(Sender : TObject; Shift : TShiftState; MousePos : TPoint; var Handled : Boolean);
    procedure StringGrid1MouseWheelUp(Sender : TObject; Shift : TShiftState; MousePos : TPoint; var Handled : Boolean);
    procedure StringGrid1Click(Sender : TObject);
    procedure EditSearchChannelChange(Sender : TObject);
    procedure PopulateCustomerData(Sender : TObject);
    procedure GetChannel(Sender : TObject);

    private
        { Private declarations }
      procedure AppBarResize;
      procedure AppBarShow(mode : Integer);
    public
        { Public declarations }
  end;

var
  CustomerForm : TCustomerForm = nil;
  oSQLCustomerGrupTable,oSQLSearchCustomerGroup,oSQL2,oSQL3,oSQL5,oSQLChannelList : TADOQuery;
  oDataSourceCustomerGrupTable,oDataSourceSearchCustomerGroup,oDataSource5,oDataSourceChannelList : TDataSource;
  sActiionCrudMode : String;
  sString                                       : String;
  Words                                         : TArray<String>;
  ChannelList,ChannelNames,arrSelectedChannelId : TArray<String>;
  sSelectedChannelId                            : string;
  wheel                                         : Boolean;
  bUpdatedChannel                               : Boolean;

implementation

{$R *.dfm}


uses uMenu;

procedure TCustomerForm.Action1Execute(Sender : TObject);
begin
  AppBarShow(-1);
end;

const
  AppBarHeight = 75;

procedure TCustomerForm.AppBarResize;
begin
  AppBar.SetBounds(0,AppBar.Parent.Height - AppBarHeight,
    AppBar.Parent.Width,AppBarHeight);
end;

procedure DeleteX(var A : TArray<String>; Index : Integer);
var
  ALength : Integer;
  I       : Integer;
begin
  ALength:=length(A);
  Assert(ALength > 0);
  Assert(Index < ALength);
  for I:=Index + 1 to ALength - 1 do
      A[I - 1]:=A[I];
  SetLength(A,ALength - 1);
end;

procedure TCustomerForm.AppBarShow(mode : Integer);
begin
  if mode = -1 then // Toggle
      mode:=Integer(not AppBar.Visible);

  if mode = 0 then
      AppBar.Visible:=False
  else
  begin
    AppBar.Visible:=True;
    AppBar.BringToFront;
  end;
end;

procedure TCustomerForm.FormCloseQuery(Sender : TObject; var CanClose : Boolean);
begin
  oSQLCustomerGrupTable.Free;
  oDataSourceCustomerGrupTable.Free
end;

procedure TCustomerForm.FormCreate(Sender : TObject);
var
  LStyle                  : TCustomStyleServices;
  MemoColor,MemoFontColor : TColor;
begin
    // Set background color for memos to the color of the form, from the active style.
  LStyle:=TStyleManager.ActiveStyle;
  MemoColor:=LStyle.GetStyleColor(scGenericBackground);
  MemoFontColor:=LStyle.GetStyleFontColor(sfButtonTextNormal);

    // Memo1.Color:=MemoColor;
    // Memo1.Font.Color:=MemoFontColor;
    // Memo2.Color:=MemoColor;
    // Memo2.Font.Color:=MemoFontColor;
    // Memo3.Color:=MemoColor;
    // Memo3.Font.Color:=MemoFontColor;
    // Memo4.Color:=MemoColor;
    // Memo4.Font.Color:=MemoFontColor;

    // Fill image

end;

procedure TCustomerForm.FormGesture(Sender : TObject;
  const EventInfo : TGestureEventInfo; var Handled : Boolean);
begin
  AppBarShow(0);
end;

procedure TCustomerForm.FormKeyDown(Sender : TObject; var Key : Word;
  Shift : TShiftState);
begin
  if Key = VK_ESCAPE then
      AppBarShow(-1)
  else
      AppBarShow(0);
end;

procedure TCustomerForm.FormResize(Sender : TObject);
begin
  AppBarResize;
end;

procedure TCustomerForm.GetChannel(Sender : TObject);
var
  oChannelId   : TADOQuery;
  oChannelName : TADOQuery;
  I            : Integer;
  j            : Integer;
  k            : Integer;
  z            : Integer;
  arrIndex     : Integer;
begin
  oChannelId:=SQLOpen('YouTubeScraping','SELECT ChannelID from CustomerChannel where CustomerId = '''+oSQLCustomerGrupTable.FieldByName('Id').AsString+'''');
  oChannelId.Open;
  try
    ChannelList:=StringReplace(oChannelId.FieldByName('ChannelID').AsString,' ',' ',[rfReplaceAll]).Split([',']);
    arrIndex:=0;
    if (length(ChannelList) <> 0) then
    begin
      for j:=0 to length(ChannelList)-1 do
      begin
        oChannelName:=SQLOpen('YouTubeScraping','SELECT ChannelTitle from Channels where Id = '''+ChannelList[j]+''' ');
        oChannelName.Open;
        try

          if (oChannelName.FieldByName('ChannelTitle').AsString <> '') then
          begin
            Inc(arrIndex);
            SetLength(ChannelNames,length(ChannelNames)+1);
            SetLength(arrSelectedChannelId,length(arrSelectedChannelId)+1);
            ChannelNames[arrIndex - 1]:=oChannelName.FieldByName('ChannelTitle').AsString;
            arrSelectedChannelId[arrIndex - 1]:=ChannelList[j];
          end;

        finally
          oChannelName.Free;
        end;
      end;

      if (length(arrSelectedChannelId) <> 0) then
      begin

        for z:=0 to length(arrSelectedChannelId)-1 do
        begin
          if (z = 0) then
          begin
            sSelectedChannelId:=arrSelectedChannelId[z];
          end
          else
          begin
            sSelectedChannelId:=sSelectedChannelId + ',' + arrSelectedChannelId[z];
          end;
        end;

      end;
    end
    else
    begin
      MemoChannelList.Text:='';
    end;

    StringGrid1.RowCount:=length(ChannelNames) + 1;
    for k:=0 to length(ChannelNames)-1 do
    begin
      StringGrid1.Rows[k+1][0]:=(k+1).ToString;
      StringGrid1.Rows[k+1][1]:=ChannelNames[k];
    end;

  finally

    oChannelId.Free;
  end;

  oSQLChannelList:=SQLOpen('YouTubeScraping','SELECT Id, ChannelTitle, ChannelID,Subscribers,Views from Channels order by Id asc');
  oSQLChannelList.Open;
  oDataSourceChannelList:=TDataSource.Create(CustomerForm);
  try
    oDataSourceChannelList.DataSet:=oSQLChannelList;
    DBGridChannel.DataSource:=oDataSourceChannelList;
  except
    on E : Exception do
    begin
      oSQLChannelList.Free;
    end;
  end;

  DBGridChannel.Columns[0].Width:=250;
  DBGridChannel.Columns[1].Width:=250;
  DBGridChannel.Columns[2].Width:=100;
  DBGridChannel.Columns[3].Width:=100;
end;

procedure TCustomerForm.PopulateCustomerData(Sender : TObject);
var
  GroupElements : TStringList;
  memoStr       : String;

begin
  oDataSourceCustomerGrupTable:=TDataSource.Create(CustomerForm);

  oSQLCustomerGrupTable:=SQLOpen('YouTubeScraping','SELECT t.Id, t.Name, t.Description, t.GroupId, t2.Name as CustomerGroup , t.Keywords FROM Customers as t INNER JOIN CustomerGroup as t2 ON t.GroupId = t2.Id order by t.Id asc');
  oSQLCustomerGrupTable.Open;
  if oSQLCustomerGrupTable.Eof then
  begin
    ShowMessage('Query returns null :' +oSQLCustomerGrupTable.SQL.Text);
    //Exit;
  end;
  oDataSourceCustomerGrupTable:=TDataSource.Create(CustomerForm);
  try
    oDataSourceCustomerGrupTable.DataSet:=oSQLCustomerGrupTable;
    DBGrid1.DataSource:=oDataSourceCustomerGrupTable;
    EditId.Text:=oSQLCustomerGrupTable.FieldByName('Id').AsString;
    EditName.Text:=oSQLCustomerGrupTable.FieldByName('Name').AsString;
    EditDescription.Text:=oSQLCustomerGrupTable.FieldByName('Description').AsString;
    MemoKeywords.Text:=oSQLCustomerGrupTable.FieldByName('Keywords').AsString;

    oSQL2:=SQLOpen('YouTubeScraping','SELECT Name FROM CustomerGroup');
    oSQL2.Open;
    try
      while not oSQL2.Eof do
      begin
        CmbCustomerGrup.AddItem(oSQL2.FieldByName('Name').AsString,Sender);
        oSQL2.Next
      end;

    except
      on E : Exception do
      begin
        oSQL2.Free;
      end;
    end;

    oSQL3:=SQLOpen('YouTubeScraping','SELECT * FROM CustomerGroup as t INNER JOIN Customers as t2 ON t.Id = t2.GroupId Where t2.GroupId = '+oSQLCustomerGrupTable.FieldByName('GroupId').AsString+'');
    oSQL3.Open;
    try
      CmbCustomerGrup.Text:=oSQL3.FieldByName('Name').AsString;
    except
      on E : Exception do
      begin
        oSQL3.Free;

      end;
    end;
  except
    on E : Exception do
    begin
      oSQLCustomerGrupTable.Free;
      oDataSourceCustomerGrupTable.Free;
    end;
  end;
  DBGrid1.Columns[2].Width:=500;
  DBGrid1.Columns[5].Width:=500;

end;

procedure TCustomerForm.FormShow(Sender : TObject);
begin
  ChannelList:=[];
  ChannelNames:=[];
  arrSelectedChannelId:=[];
  wheel:=False;
  bUpdatedChannel:=False;
  MemoKeywords.Text:='';
  MemoChannelList.Text:='';
  EditSearchChannel.Text:='';
  StringGrid1.Rows[0].CommaText:='No,Channel';
  StringGrid1.ColWidths[0]:=50;
  StringGrid1.ColWidths[1]:=400;

  PopulateCustomerData(Sender);
  GetChannel(Sender);

end;

procedure TCustomerForm.MemoKeywordsChange(Sender : TObject);
begin
  if (MemoKeywords.Text = '') then
  begin
    sString:='';
  end;
end;

procedure TCustomerForm.MemoKeywordsKeyPress(Sender : TObject; var Key : Char);
var
  Words1     : TArray<String>;
  I          : Integer;
  sOld       : String;
  sNewString : String;
begin
  if (Key=#13) or (Key = #44) then
  begin
    Key:=#0;

    if (length(Words) < 1) then
    begin
      sString:=Copy(MemoKeywords.Text,LastDelimiter(',',MemoKeywords.Text) + 1,length(MemoKeywords.Text));
      if (sString = '') then
      begin
        ShowMessage('plase add keyword before pressing enter');
      end
      else
      begin
        Words:=StringReplace(MemoKeywords.Text,' ',' ',[rfReplaceAll]).Split([',']);
        MemoKeywords.Text:=MemoKeywords.Text + ',';
        MemoKeywords.SelStart:=length(MemoKeywords.Text);
      end;

    end
    else
    begin
      sString:=Copy(MemoKeywords.Text,LastDelimiter(',',MemoKeywords.Text) + 1,length(MemoKeywords.Text));
      if (sString = '') then
      begin
        ShowMessage('plase add keyword before pressing enter');
      end
      else
      begin
        if MatchText(sString,Words) then
        begin
          ShowMessage('Duplicated Keyword!');
          for I:=0 to length(Words)-1 do
          begin
            if (I = 0) then
            begin
              MemoKeywords.Text:=Words[I];
            end
            else
            begin
              MemoKeywords.Text:=MemoKeywords.Text+','+Words[I];
            end;

          end;
          MemoKeywords.Text:=MemoKeywords.Text + ',';
          MemoKeywords.SelStart:=length(MemoKeywords.Text);
        end
        else
        begin

          Words:=StringReplace(MemoKeywords.Text,' ',' ',[rfReplaceAll]).Split([',']);
          MemoKeywords.Text:=MemoKeywords.Text + ',';
          MemoKeywords.SelStart:=length(MemoKeywords.Text);
        end;
      end;

    end;

  end;

end;

procedure TCustomerForm.StringGrid1Click(Sender : TObject);
var
  iStringGridRowId,I,k,Index,z : Integer;
  sStringGridChannelName       : String;
  arrays                       : TArray<String>;
  Button                       : TMouseButton;
begin
  if ((length(ChannelNames) <> 0) and (wheel = False)) then
  begin
    bUpdatedChannel:=True;
    iStringGridRowId:=StrToInt(StringGrid1.Cells[0,StringGrid1.Row]);
    sStringGridChannelName:=StringGrid1.Cells[1,StringGrid1.Row];
    DeleteX(ChannelNames,(iStringGridRowId-1));
    DeleteX(arrSelectedChannelId,(iStringGridRowId-1));

    for I:=3 to StringGrid1.ColCount - 1 do
        StringGrid1.Cols[I].Clear;
    StringGrid1.RowCount:=length(ChannelNames)+1;

    for k:=0 to length(ChannelNames)-1 do
    begin

      if (length(ChannelNames) > StringGrid1.RowCount) then
      begin
        StringGrid1.RowCount:=length(ChannelNames)+1;
      end;

      StringGrid1.Rows[k+1][0]:=(k+1).ToString;
      StringGrid1.Rows[k+1][1]:=ChannelNames[k];
    end;
  end;

end;

procedure TCustomerForm.StringGrid1MouseDown(Sender : TObject; Button : TMouseButton; Shift : TShiftState; X,Y : Integer);
begin
  wheel:=False;
end;

procedure TCustomerForm.StringGrid1MouseWheelDown(Sender : TObject; Shift : TShiftState; MousePos : TPoint; var Handled : Boolean);
begin
  wheel:=True;
end;

procedure TCustomerForm.StringGrid1MouseWheelUp(Sender : TObject; Shift : TShiftState; MousePos : TPoint; var Handled : Boolean);
begin
  wheel:=True;
end;

procedure TCustomerForm.BackToMainForm(Sender : TObject);
begin
  oSQLCustomerGrupTable.Free;
  oSQLSearchCustomerGroup.Free;
  oSQL2.Free;
  oSQL3.Free;
  oSQL5.Free;
  oSQLChannelList.Free;
  oDataSourceCustomerGrupTable.Free;
  oDataSourceSearchCustomerGroup.Free;
  oDataSource5.Free;
  oDataSourceChannelList.Free;
    // BtnCancel.Click;
  ChannelList:=[];
  ChannelNames:=[];
  arrSelectedChannelId:=[];
  CmbCustomerGrup.Clear;
  EditSearch.Text:='';
  Hide;
  GridForm.BringToFront;
end;

procedure TCustomerForm.BtnAddClick(Sender : TObject);
var
  I : Integer;
begin
  ChannelList:=[];
  ChannelNames:=[];
  arrSelectedChannelId:=[];
  sSelectedChannelId:='';
  for I:=3 to StringGrid1.ColCount - 1 do
      StringGrid1.Cols[I].Clear;
  StringGrid1.RowCount:=1;
  BtnSave.Enabled:=True;
  BtnCancel.Enabled:=True;
  EditDescription.Enabled:=True;
  EditName.Enabled:=True;
  BtnAdd.Enabled:=False;
  BtnUpdate.Enabled:=False;
  BtnDelete.Enabled:=False;
  CmbCustomerGrup.Enabled:=True;
  MemoKeywords.Enabled:=True;
  EditName.SetFocus;
  EditId.Text:='';
  EditName.Text:='';
  EditDescription.Text:='';
  MemoKeywords.Text:='';
  CmbCustomerGrup.Text:='';
  DBGrid1.Enabled:=False;
  DBGridChannel.Enabled:=True;
  MemoChannelList.Enabled:=True;
  sActiionCrudMode:='add';
  StringGrid1.Enabled:=True;
  StringGrid1.Cursor:=crHandPoint;

end;

procedure TCustomerForm.BtnCancelClick(Sender : TObject);
begin
  StringGrid1.Enabled:=False;
  BtnSave.Enabled:=False;
  BtnCancel.Enabled:=False;
  EditDescription.Enabled:=False;
  EditName.Enabled:=False;
  BtnAdd.Enabled:=True;
  BtnUpdate.Enabled:=True;
  BtnDelete.Enabled:=True;
  MemoKeywords.Enabled:=False;
  CmbCustomerGrup.Enabled:=False;
  if (DBGrid1.Enabled = False) then
  begin
    DBGrid1.Enabled:=True;
  end;
  EditId.Text:=oSQLCustomerGrupTable.FieldByName('Id').AsString;
  EditName.Text:=oSQLCustomerGrupTable.FieldByName('Name').AsString;
  EditDescription.Text:=oSQLCustomerGrupTable.FieldByName('Description').AsString;
  sActiionCrudMode:='none';
  if (BtnSave.Caption = 'Delete') then
  begin
    BtnSave.Caption:='Save';
  end
  else if (BtnSave.Caption = 'Update') then
  begin
    BtnSave.Caption:='Save';
  end;
  DBGridChannel.Enabled:=False;
  MemoChannelList.Enabled:=False;
  oSQLCustomerGrupTable.Free;
  oSQLSearchCustomerGroup.Free;
  oSQL2.Free;
  oSQL3.Free;
  oSQL5.Free;
  oSQLChannelList.Free;
  oDataSourceCustomerGrupTable.Free;
  oDataSourceSearchCustomerGroup.Free;
  oDataSource5.Free;
  oDataSourceChannelList.Free;
  ChannelList:=[];
  ChannelNames:=[];
  arrSelectedChannelId:=[];
  CmbCustomerGrup.Clear;
  FormShow(Sender);
  if (EditSearch.Text <> '') then
  begin
    EditSearch.Text:='';
  end;

end;

procedure TCustomerForm.BtnDeleteClick(Sender : TObject);
begin
  BtnSave.Caption:='Delete';
  sActiionCrudMode:='delete';
  BtnSave.Enabled:=True;
  BtnCancel.Enabled:=True;
  BtnAdd.Enabled:=False;
  BtnUpdate.Enabled:=False;
  BtnDelete.Enabled:=False;

end;

procedure TCustomerForm.BtnSaveClick(Sender : TObject);
var
  oSQL         : TADOQuery;
  sName        : String;
  sDescription : String;
  oGetGroupId  : TADOQuery;
  sGroupId     : String;

  sKeywords      : String;
  sAddedId       : String;
  strUpdateQuery : string;
  nExecQuery,z   : Integer;

begin
  if (sActiionCrudMode = 'add') then
  begin
    if (EditName.Text = '') then
    begin
      application.messageBox('Name can not be empty !','Warning',MB_ICONWARNING);
      EditName.SetFocus;
    end
    else
    begin
      sKeywords:=MemoKeywords.Text;
      if (sKeywords[length(sKeywords)] = ',') then
      begin
        delete(sKeywords,length(sKeywords),1);
      end;
      try
        oGetGroupId:=SQLOpen('YouTubeScraping','Select Id from CustomerGroup where Name ='''+CmbCustomerGrup.Text+''' ');
        oGetGroupId.Open;
        try

          sGroupId:=oGetGroupId.FieldByName('Id').AsString;

        except
          on E : Exception do
          begin
            oGetGroupId.Free;
          end;
        end;
        oSQL:=SQLOpen('YouTubeScraping','Select * from Customers Where 1=2');
        oSQL.Insert;
        oSQL.FieldByName('Name').AsString:=EditName.Text;
        oSQL.FieldByName('Description').AsString:=EditDescription.Text;
        oSQL.FieldByName('GroupId').AsString:=sGroupId;
        oSQL.FieldByName('Keywords').AsString:=sKeywords;
        oSQL.Post;

      finally
        oGetGroupId.Free;
        oSQL.Free;
      end;

      try
        oSQL:=SQLOpen('YouTubeScraping','Select TOP 1 Id from Customers ORDER BY Id DESC');
        oSQL.Open;
        try
          sAddedId:=oSQL.FieldByName('Id').AsString;

        except
          on E : Exception do
          begin
            oGetGroupId.Free;
          end;
        end;
      finally
        oSQL.Free;
      end;

      if (bUpdatedChannel = True) then
      begin
        for z:=0 to length(arrSelectedChannelId)-1 do
        begin
          if (z = 0) then
          begin
            sSelectedChannelId:=arrSelectedChannelId[z];
          end
          else
          begin
            sSelectedChannelId:=sSelectedChannelId + ',' + arrSelectedChannelId[z];
          end;
        end;
        if sSelectedChannelId.Contains('''') then
        begin
          sSelectedChannelId:=StringReplace(sSelectedChannelId,'''','''''',[rfReplaceAll])
        end;
      end;

      try
        oSQL:=SQLOpen('YouTubeScraping','Select * from CustomerChannel Where 1=2');
        oSQL.Insert;
        oSQL.FieldByName('CustomerId').AsString:=sAddedId;

        oSQL.FieldByName('ChannelID').AsString:=sSelectedChannelId;

        oSQL.Post;
        ShowMessage('Data Saved!');
        DBGridChannel.Enabled:=False;
        MemoChannelList.Enabled:=False;
        BtnSave.Enabled:=False;
        BtnCancel.Enabled:=False;
        BtnAdd.Enabled:=True;
        BtnUpdate.Enabled:=True;
        BtnDelete.Enabled:=True;
        if (DBGrid1.Enabled = False) then
        begin
          DBGrid1.Enabled:=True;
        end;
        oSQLCustomerGrupTable.Free;
        oSQLSearchCustomerGroup.Free;
        oSQL2.Free;
        oSQL3.Free;
        oSQL5.Free;
        oSQLChannelList.Free;
        oDataSourceCustomerGrupTable.Free;
        oDataSourceSearchCustomerGroup.Free;
        oDataSource5.Free;
        oDataSourceChannelList.Free;
        FormShow(Sender);
      finally
        oSQL.Free;
        sAddedId:='';
      end;
    end;

  end
  else
    if (sActiionCrudMode = 'delete') then
  begin
    if (messagedlg('are you sure want to delete this data ?',mtConfirmation,
        [mbYes,mbCancel],0) = mrYes) then
    begin
      SQLExec('YouTubeScraping','DELETE from Customers Where Id ='''+EditId.Text+'''');
      ShowMessage('Data has been deleted!');
      oSQLCustomerGrupTable.Free;
      oSQLSearchCustomerGroup.Free;
      oSQL2.Free;
      oSQL3.Free;
      oSQL5.Free;
      oSQLChannelList.Free;
      oDataSourceCustomerGrupTable.Free;
      oDataSourceSearchCustomerGroup.Free;
      oDataSource5.Free;
      oDataSourceChannelList.Free;
      BtnSave.Enabled:=False;
      BtnCancel.Enabled:=False;
      BtnAdd.Enabled:=True;
      BtnUpdate.Enabled:=True;
      BtnDelete.Enabled:=True;
      BtnSave.Caption:='Save';

      FormShow(Sender);
    end;
  end
  else
    if (sActiionCrudMode = 'update') then
  begin
    if (EditName.Text = '') then
    begin
      application.messageBox('Name can not be empty !','Warning',MB_ICONWARNING);
      EditName.SetFocus;
    end
    else
    begin
      sSelectedChannelId:='';
      sDescription:=EditDescription.Text;
      sKeywords:=MemoKeywords.Text;
      sName:=EditName.Text;
      if (sKeywords[length(sKeywords)] = ',') then
      begin
        delete(sKeywords,length(sKeywords),1);
      end;

      if sDescription.Contains('''') then
      begin
        sDescription:=StringReplace(sDescription,'''','''''',[rfReplaceAll])
      end;

      if sKeywords.Contains('''') then
      begin
        sKeywords:=StringReplace(sKeywords,'''','''''',[rfReplaceAll])
      end;

      if sName.Contains('''') then
      begin
        sName:=StringReplace(sName,'''','''''',[rfReplaceAll])
      end;

      SQLExec('YouTubeScraping','UPDATE Customers SET'
          +' Name= '''+sName+''' '+
          ',Description='''+sDescription+''' '+
          ',Keywords='''+sKeywords+''' '+
          ' where Id='''+EditId.Text+''' ');

      if (bUpdatedChannel = True) then
      begin
        for z:=0 to length(arrSelectedChannelId)-1 do
        begin
          if (z = 0) then
          begin
            sSelectedChannelId:=arrSelectedChannelId[z];
          end
          else
          begin
            sSelectedChannelId:=sSelectedChannelId + ',' + arrSelectedChannelId[z];
          end;
        end;
        if sSelectedChannelId.Contains('''') then
        begin
          sSelectedChannelId:=StringReplace(sSelectedChannelId,'''','''''',[rfReplaceAll])
        end;
        SQLExec('YouTubeScraping','UPDATE CustomerChannel SET'
            +' ChannelID= '''+sSelectedChannelId+''' WHERE CustomerId = '''+EditId.Text+'''  ');
      end;

      application.messageBox('Data Updated!','Success',MB_OK);

      StringGrid1.Cursor:=crNo;
      StringGrid1.Enabled:=False;
      DBGridChannel.Enabled:=False;
      MemoChannelList.Enabled:=False;
      BtnSave.Enabled:=False;
      BtnCancel.Enabled:=False;
      BtnAdd.Enabled:=True;
      BtnUpdate.Enabled:=True;
      BtnDelete.Enabled:=True;
      EditDescription.Enabled:=False;
      EditName.Enabled:=False;
      EditId.Enabled:=False;
      BtnSave.Caption:='Save';
      oSQLCustomerGrupTable.Free;
      oSQLSearchCustomerGroup.Free;
      oSQL2.Free;
      oSQL3.Free;
      oSQL5.Free;
      oSQLChannelList.Free;
      oDataSourceCustomerGrupTable.Free;
      oDataSourceSearchCustomerGroup.Free;
      oDataSource5.Free;
      oDataSourceChannelList.Free;
      FormShow(Sender);
      if (DBGrid1.Enabled = False) then
      begin
        DBGrid1.Enabled:=True;
      end;
      if (EditSearch.Text <> '') then
      begin
        EditSearch.Text:='';
      end;
    end;
  end;
end;

procedure TCustomerForm.BtnUpdateClick(Sender : TObject);
begin
  sActiionCrudMode:='update';
  BtnSave.Enabled:=True;
  BtnCancel.Enabled:=True;
  BtnAdd.Enabled:=False;
  BtnUpdate.Enabled:=False;
  BtnDelete.Enabled:=False;
  MemoKeywords.Enabled:=True;
  CmbCustomerGrup.Enabled:=True;
  EditName.Enabled:=True;
  EditDescription.Enabled:=True;
  CmbCustomerGrup.Enabled:=False;
  Words:=StringReplace(MemoKeywords.Text,' ',' ',[rfReplaceAll]).Split([',']);
  MemoKeywords.Text:=MemoKeywords.Text + ',';
  MemoKeywords.SelStart:=length(MemoKeywords.Text);
  DBGridChannel.Enabled:=True;
  MemoChannelList.Enabled:=True;
  BtnSave.Caption:='Update';
  StringGrid1.Enabled:=True;
  StringGrid1.Cursor:=crHandPoint;
end;

procedure TCustomerForm.DBGrid1CellClick(Column : TColumn);
var
  oSQL,oChannelId,oChannelName : TADOQuery;
  j,k,arrIndex,I,z             : Integer;
begin
  if (EditSearch.Text = '') then
  begin
    ChannelList:=[];
    ChannelNames:=[];
    arrSelectedChannelId:=[];
    for I:=3 to StringGrid1.ColCount - 1 do
        StringGrid1.Cols[I].Clear;
    StringGrid1.RowCount:=1;

    MemoChannelList.Text:='';
    EditId.Text:=oSQLCustomerGrupTable.FieldByName('Id').AsString;
    EditName.Text:=oSQLCustomerGrupTable.FieldByName('Name').AsString;
    EditDescription.Text:=oSQLCustomerGrupTable.FieldByName('Description').AsString;
    MemoKeywords.Text:=oSQLCustomerGrupTable.FieldByName('Keywords').AsString;
    oSQL:=SQLOpen('YouTubeScraping','SELECT * FROM CustomerGroup as t INNER JOIN Customers as t2 ON t.Id = t2.GroupId Where t2.GroupId = '+oSQLCustomerGrupTable.FieldByName('GroupId').AsString+'');
    oSQL.Open;
    try

      CmbCustomerGrup.Text:=oSQL.FieldByName('Name').AsString;

    except
      on E : Exception do
      begin
        oSQL.Free;
      end;
    end;
    oSQL.Free;

    oChannelId:=SQLOpen('YouTubeScraping','SELECT ChannelID from CustomerChannel where CustomerId = '''+oSQLCustomerGrupTable.FieldByName('Id').AsString+'''');
    oChannelId.Open;
    try

      ChannelList:=StringReplace(oChannelId.FieldByName('ChannelID').AsString,' ',' ',[rfReplaceAll]).Split([',']);

      arrIndex:=0;
      if (length(ChannelList) <> 0) then
      begin
        for j:=0 to length(ChannelList)-1 do
        begin

          oChannelName:=SQLOpen('YouTubeScraping','SELECT ChannelTitle from Channels where Id = '''+ChannelList[j]+''' ');
          oChannelName.Open;
          try

            if (oChannelName.FieldByName('ChannelTitle').AsString <> '') then
            begin
              Inc(arrIndex);

              SetLength(ChannelNames,length(ChannelNames)+1);
              SetLength(arrSelectedChannelId,length(arrSelectedChannelId)+1);

              ChannelNames[arrIndex - 1]:=oChannelName.FieldByName('ChannelTitle').AsString;
              arrSelectedChannelId[arrIndex - 1]:=ChannelList[j];
            end;

          finally
            oChannelName.Free;
          end;
        end;

        if (length(arrSelectedChannelId) <> 0) then
        begin

          for z:=0 to length(arrSelectedChannelId)-1 do
          begin
            if (z = 0) then
            begin
              sSelectedChannelId:=arrSelectedChannelId[z];
            end
            else
            begin
              sSelectedChannelId:=sSelectedChannelId + ',' + arrSelectedChannelId[z];
            end;
          end;

        end;

      end
      else
      begin
        MemoChannelList.Text:='';
      end;

      StringGrid1.RowCount:=length(ChannelNames) + 1;
      for k:=0 to length(ChannelNames)-1 do
      begin
        StringGrid1.Rows[k+1][0]:=(k+1).ToString;
        StringGrid1.Rows[k+1][1]:=ChannelNames[k];
      end;

    finally

      oChannelId.Free;
    end;
  end
  else
  begin
    ChannelList:=[];
    ChannelNames:=[];
    arrSelectedChannelId:=[];
    for I:=3 to StringGrid1.ColCount - 1 do
        StringGrid1.Cols[I].Clear;
    StringGrid1.RowCount:=1;
    DBGrid1.DataSource:=oDataSourceSearchCustomerGroup;
    EditId.Text:=oSQLSearchCustomerGroup.FieldByName('Id').AsString;
    EditName.Text:=oSQLSearchCustomerGroup.FieldByName('Name').AsString;
    EditDescription.Text:=oSQLSearchCustomerGroup.FieldByName('Description').AsString;
    MemoKeywords.Text:=oSQLSearchCustomerGroup.FieldByName('Keywords').AsString;
    oSQL:=SQLOpen('YouTubeScraping','SELECT * FROM CustomerGroup as t INNER JOIN Customers as t2 ON t.Id = t2.GroupId Where t2.GroupId = '+oSQLSearchCustomerGroup.FieldByName('GroupId').AsString+'');
    oSQL.Open;
    try

      CmbCustomerGrup.Text:=oSQL.FieldByName('Name').AsString;

    except
      on E : Exception do
      begin
        oSQL.Free;
      end;
    end;
    oSQL.Free;

    oChannelId:=SQLOpen('YouTubeScraping','SELECT ChannelID from CustomerChannel where CustomerId = '''+oSQLSearchCustomerGroup.FieldByName('Id').AsString+'''');
    oChannelId.Open;
    try

      ChannelList:=StringReplace(oChannelId.FieldByName('ChannelID').AsString,' ',' ',[rfReplaceAll]).Split([',']);

      arrIndex:=0;
      if (length(ChannelList) <> 0) then
      begin
        for j:=0 to length(ChannelList)-1 do
        begin

          oChannelName:=SQLOpen('YouTubeScraping','SELECT ChannelTitle from Channels where Id = '''+ChannelList[j]+''' ');
          oChannelName.Open;
          try

            if (oChannelName.FieldByName('ChannelTitle').AsString <> '') then
            begin
              Inc(arrIndex);

              SetLength(ChannelNames,length(ChannelNames)+1);
              SetLength(arrSelectedChannelId,length(arrSelectedChannelId)+1);

              ChannelNames[arrIndex - 1]:=oChannelName.FieldByName('ChannelTitle').AsString;
              arrSelectedChannelId[arrIndex - 1]:=ChannelList[j];
            end;

          finally
            oChannelName.Free;
          end;
        end;

        if (length(arrSelectedChannelId) <> 0) then
        begin

          for z:=0 to length(arrSelectedChannelId)-1 do
          begin
            if (z = 0) then
            begin
              sSelectedChannelId:=arrSelectedChannelId[z];
            end
            else
            begin
              sSelectedChannelId:=sSelectedChannelId + ',' + arrSelectedChannelId[z];
            end;
          end;

        end;

      end
      else
      begin
        MemoChannelList.Text:='';
      end;

      StringGrid1.RowCount:=length(ChannelNames) + 1;
      for k:=0 to length(ChannelNames)-1 do
      begin
        StringGrid1.Rows[k+1][0]:=(k+1).ToString;
        StringGrid1.Rows[k+1][1]:=ChannelNames[k];
      end;

    finally

      oChannelId.Free;
    end;
  end;
end;

procedure TCustomerForm.DBGridChannelCellClick(Column : TColumn);
var
  sChannelTitle     : string;
  I                 : Integer;
  sChannelId        : string;
  bRenderStringGrid : Boolean;
begin
  bRenderStringGrid:=True;
  sChannelTitle:=oSQLChannelList.FieldByName('ChannelTitle').AsString;

  for I:=0 to length(arrSelectedChannelId)-1 do
  begin
    if ((arrSelectedChannelId[I] = oSQLChannelList.FieldByName('Id').AsString)AND (length(arrSelectedChannelId[I])<>0)) then
    begin
      ShowMessage('Selected channel is allready on the list!');
      bRenderStringGrid:=False;
    end;

  end;

  if (bRenderStringGrid = True) then
  begin
    bUpdatedChannel:=True;

    SetLength(ChannelNames,length(ChannelNames)+1);
    SetLength(arrSelectedChannelId,length(arrSelectedChannelId)+1);

    ChannelNames[length(ChannelNames)-1]:=sChannelTitle;
    StringGrid1.RowCount:=StringGrid1.RowCount + 1;
    StringGrid1.Rows[length(ChannelNames)][0]:=(length(ChannelNames)).ToString;
    StringGrid1.Rows[length(ChannelNames)][1]:=sChannelTitle;

    arrSelectedChannelId[length(arrSelectedChannelId)-1]:=oSQLChannelList.FieldByName('Id').AsString;
  end;
end;

procedure DrawCheckBoxes(oGrid : TObject; Rect : TRect; Column : TColumn);
var
  MyRect  : TRect;
  oField  : TField;
  iPos    : Integer;
  iFactor : Integer;

  bValue : Boolean;
begin
  with (oGrid as TDBGrid) do
  begin
    oField:=Column.Field;
    Canvas.FillRect(Rect);

    MyRect.Top:=((Rect.Bottom - Rect.Top - 11) div 2) + Rect.Top;
    MyRect.Left:=((Rect.Right - Rect.Left - 11) div 2) + Rect.Left;
    MyRect.Bottom:=MyRect.Top + 10;
    MyRect.Right:=MyRect.Left + 10;

    Canvas.Pen.Color:=clBlack;

    Canvas.Polyline([
        Point(MyRect.Left,MyRect.Top),Point(MyRect.Right,MyRect.Top),
        Point(MyRect.Right,MyRect.Bottom),Point(MyRect.Left,MyRect.Bottom),
        Point(MyRect.Left,MyRect.Top)]);

    iPos:=MyRect.Left;
    iFactor:=1;

    if bValue then
    begin
      Canvas.MoveTo(iPos + (iFactor*2),MyRect.Top + 4);
      Canvas.LineTo(iPos + (iFactor*2),MyRect.Top + 7);
      Canvas.MoveTo(iPos + (iFactor*3),MyRect.Top + 5);
      Canvas.LineTo(iPos + (iFactor*3),MyRect.Top + 8);
      Canvas.MoveTo(iPos + (iFactor*4),MyRect.Top + 6);
      Canvas.LineTo(iPos + (iFactor*4),MyRect.Top + 9);
      Canvas.MoveTo(iPos + (iFactor*5),MyRect.Top + 5);
      Canvas.LineTo(iPos + (iFactor*5),MyRect.Top + 8);
      Canvas.MoveTo(iPos + (iFactor*6),MyRect.Top + 4);
      Canvas.LineTo(iPos + (iFactor*6),MyRect.Top + 7);
      Canvas.MoveTo(iPos + (iFactor*7),MyRect.Top + 3);
      Canvas.LineTo(iPos + (iFactor*7),MyRect.Top + 6);
      Canvas.MoveTo(iPos + (iFactor*8),MyRect.Top + 2);
      Canvas.LineTo(iPos + (iFactor*8),MyRect.Top + 5);
    end;
  end;
end;

  // procedure TCustomerForm.DBGridChannelDblClick(Sender : TObject);
  // begin
  // if DBGridChannel.SelectedField.FieldName = 'Active' then
  // begin
  //
  // end;
  // end;

  // procedure TCustomerForm.DBGridChannelDrawColumnCell(Sender : TObject; const Rect : TRect; DataCol : Integer; Column : TColumn; State : TGridDrawState);
  // begin
  // if Column.Title.Caption = 'Active' then // The field name that has "Y" or "N".
  // DrawCheckBoxes(Sender,Rect,Column);
  // end;

procedure TCustomerForm.EditSearchChange(Sender : TObject);
begin
  oSQLSearchCustomerGroup:=SQLOpen('YouTubeScraping','Select t.Id, t.Name, t.Description, t.GroupId, t2.Name as CustomerGroup'+
      ' , t.Keywords FROM Customers as t'+
      ' INNER JOIN CustomerGroup as t2 ON t.GroupId = t2.Id'+
      ' where t.Name LIKE ''%'+EditSearch.Text+'%'''+
      ' or t.Description LIKE ''%'+EditSearch.Text+'%''' +
      ' or t.GroupId LIKE ''%'+EditSearch.Text+'%''' +
      ' or t.Keywords LIKE ''%'+EditSearch.Text+'%''' +
      ' or t2.Name LIKE ''%'+EditSearch.Text+'%''' +
      ' or t.Id LIKE ''%'+EditSearch.Text+'%'' ');
  oSQLSearchCustomerGroup.Open;
  oDataSourceSearchCustomerGroup:=TDataSource.Create(CustomerForm);

  try

    oDataSourceSearchCustomerGroup.DataSet:=oSQLSearchCustomerGroup;
    DBGrid1.DataSource:=oDataSourceSearchCustomerGroup;
    if (EditSearch.Text = '') then
    begin
      DBGrid1.DataSource:=oDataSourceCustomerGrupTable;
    end;

  except
    on E : Exception do
    begin
      oSQLSearchCustomerGroup.Free;
    end;
  end;
  DBGrid1.Columns[2].Width:=500;
  DBGrid1.Columns[5].Width:=500;
end;

procedure TCustomerForm.EditSearchChannelChange(Sender : TObject);
var
  oSQL        : TADOQuery;
  oDataSource : TDataSource;
begin

  oSQL:=SQLOpen('YouTubeScraping','Select Id, ChannelTitle, ChannelID,Subscribers,Views  from Channels where'+
      ' ChannelTitle  LIKE ''%'+EditSearchChannel.Text+'%'' ');
  oSQL.Open;
  oDataSource:=TDataSource.Create(CustomerForm);

  try

    oDataSource.DataSet:=oSQL;
    DBGridChannel.DataSource:=oDataSource;
    if (EditSearchChannel.Text = '') then
    begin
      DBGridChannel.DataSource:=oDataSourceChannelList;
      oSQL.Free;
      oDataSource.Free;
    end;

  except
    on E : Exception do
    begin
      oSQLSearchCustomerGroup.Free;
    end;
  end;

end;

end.
