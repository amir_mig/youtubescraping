unit uHelperData;

interface

uses
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.StrUtils,
  System.Variants,
  System.Classes,
  System.IniFiles,
  System.JSON,
  System.NetEncoding,
  MSXml,
  uSQL, uModel,
  Data.DB,
  Data.Win.ADODB, uHelperApi;

procedure SaveComment(comment: TComment);
procedure SaveCommentsToDB(comments: TComments);
procedure SaveVideo(video: TVideo);
procedure SaveLanguage(language: TLanguage);
function GetCustomerList(lCustomer: TStringList): integer;
function GetChannelList(lChannel: TStringList): integer;
procedure GetChannelByCustomer(CustId: string; lChannelByCustomer: TStringList);
procedure GetKeywords(CustId: String; lKeyword: TStringList);

implementation

procedure SaveCommentsToDB(comments: TComments);

var
  I: integer;
begin
  for I := 0 to Length(comments) - 1 do
  begin
    SaveComment(comments[I]);
  end;
end;

procedure SaveComment(comment: TComment);
var
  oSQL: TADOQuery;
  sQuery: string;
begin
  try
    oSQL := SQLOpen('YoutubeScraping',
      'Select id from VideoComments where commentID =' +
      QuotedStr(comment.CommentId));
    while not oSQL.Eof do
      Exit;
    sQuery := 'INSERT INTO VideoComments (VideoID,AuthorDisplayName,AuthorProfileImageUrl,TextDisplay,CommentId,TextOriginal,PublishedAt,UpdatedAt) Values( ';
    sQuery := sQuery + QuotedStr(comment.VideoId) + ',';
    sQuery := sQuery + QuotedStr(comment.AuthorDisplayName) + ',';
    sQuery := sQuery + QuotedStr(comment.AuthorProfileImageUrl) + ',';
    sQuery := sQuery + QuotedStr(comment.TextDisplay) + ',';
    sQuery := sQuery + QuotedStr(comment.CommentId) + ',';
    sQuery := sQuery + QuotedStr(comment.TextOriginal) + ',';
    sQuery := sQuery + QuotedStr(comment.PublishedAt) + ',';
    sQuery := sQuery + QuotedStr(comment.UpdatedAt) + ')';
    if sQuery <> '' then
      SQLExec('YoutubeScraping', sQuery);

  finally
    oSQL.Free;
  end;
end;

procedure SaveVideo(video: TVideo);
var
  oSQL: TADOQuery;
  sQuery: string;
  lLanguage: TLanguage;
begin

  try
    oSQL := SQLOpen('YoutubeScraping', 'Select id from Videos where VideoID =' +
      QuotedStr(video.VideoId));
    while not oSQL.Eof do
      Exit;

    sQuery := 'Insert Into Videos (CategoryId,Keyword,Title,Descriptions,Tags,Likes,Dislikes,Views,Favorites,Comments,Duration,ChannelId,CustomerId,IsValid,PublishedDate,BatchId,VideoID) Values(';
    sQuery := sQuery + QuotedStr(video.CategoryId) + ',';
    sQuery := sQuery + QuotedStr(video.Keyword) + ',';
    sQuery := sQuery + QuotedStr(video.Title) + ',';
    sQuery := sQuery + QuotedStr(video.Description) + ',';
    sQuery := sQuery + QuotedStr(video.Tags) + ',';
    sQuery := sQuery + QuotedStr(video.Likes) + ',';
    sQuery := sQuery + QuotedStr(video.DisLikes) + ',';
    sQuery := sQuery + QuotedStr(video.Views) + ',';
    sQuery := sQuery + QuotedStr(video.Favorites) + ',';
    sQuery := sQuery + QuotedStr(video.comments) + ',';
    sQuery := sQuery + QuotedStr(video.Duration) + ',';
    sQuery := sQuery + QuotedStr(video.ChannelId) + ',';
    sQuery := sQuery + QuotedStr(video.CustomerId) + ',';
    sQuery := sQuery + '0,';
    sQuery := sQuery + QuotedStr(video.PublishedDate) + ',';
    sQuery := sQuery + QuotedStr(video.BatchId) + ',';
    sQuery := sQuery + QuotedStr(video.VideoId) + ')';
    // oSQL:=TADOQuery.Create(nil);
    if video.VideoId <> '' then
      SQLExec('YoutubeScraping', sQuery);
    if Length(video.languages) > 0 then
      for lLanguage in video.languages do
      begin
        SaveLanguage(lLanguage);
      end;
  finally
    oSQL.Free;
  end;
end;

procedure SaveLanguage(language: TLanguage);
var

  sQuery: string;
begin
  sQuery := 'Insert Into Languages (Code,Reliable, Confidence,VideoId) Values(';
  sQuery := sQuery + QuotedStr(language.code) + ',';
  sQuery := sQuery + QuotedStr(language.reliable) + ',';
  sQuery := sQuery + QuotedStr(language.Confidence) + ',';
  sQuery := sQuery + QuotedStr(language.VideoId) + ')';
  if language.code <> '' then
    SQLExec('YoutubeScraping', sQuery);

end;

function GetCustomerList(lCustomer: TStringList): integer;
var
  oSQL: TADOQuery;

begin
  try
    oSQL := SQLOpen('YouTubeScraping', 'SELECT customer.Id as CustId ,' +
      ' customer.Name as CustName ,' + ' custGroup.Name as custGrupName' +
      ' from [dbo].[Customers] as customer' +
      ' INNER JOIN [dbo].[CustomerGroup] as custGroup ON customer.GroupId = custGroup.Id');
    oSQL.Recordset.MoveFirst;
    while not oSQL.Eof do
    begin
      lCustomer.AddPair(' [ ' + oSQL.Recordset.Fields[2].Value + ' ]- ' +
        oSQL.Recordset.Fields[1].Value, oSQL.Recordset.Fields[0].Value);
      oSQL.Recordset.MoveNext;
      if oSQL.Recordset.Eof then
        Exit;
    end;
    result := oSQL.RecordCount;
  finally
    oSQL.Free;
  end;
end;

function GetChannelList(lChannel: TStringList): integer;
var
  oSQL: TADOQuery;

begin

  oSQL := SQLOpen('YoutubeScraping',
    'Select Channelid, ChannelTitle From Channels');
  try
    oSQL.Recordset.MoveFirst;
    while not oSQL.Eof do
    begin
      lChannel.AddPair(oSQL.Recordset.Fields[1].Value,
        oSQL.Recordset.Fields[0].Value);
      oSQL.Recordset.MoveNext;
      if oSQL.Recordset.Eof then
        Exit;
    end;
    result := oSQL.RecordCount;
  finally
    oSQL.Free;
  end;

end;

procedure GetChannelByCustomer(CustId: string; lChannelByCustomer: TStringList);
var
  oSQL: TADOQuery;
  sChannel: String;
  arrayChannel: TArray<String>;
  I, j: integer;
  lChannel: TStringList;
  sSQL: string;

begin
  sSQL := 'SELECT CH.ChannelTitle , CH.ChannelID FROM Channels CH ' +
    ' ,CustomerChannel CC where CH.Id in  (SELECT value FROM STRING_SPLIT(CC.ChannelId, '',''))  ' +
    ' and CC.CustomerId = ' + CustId;
  try
    oSQL := SQLOpen('YouTubeScraping', sSQL);
    while not oSQL.Eof do
    begin
      lChannelByCustomer.AddPair(oSQL.FieldByName('ChannelTitle').Value,
        oSQL.FieldByName('ChannelID').Value);
      oSQL.Recordset.MoveNext;
      if oSQL.Recordset.Eof then
        Exit;
    end;
  finally
    oSQL.Free;
  end;

end;

procedure GetKeywords(CustId: String; lKeyword: TStringList);
var
  oSQL: TADOQuery;
  I: integer;
  sKeywords: String;
  arrayKeyword: TArray<String>;

begin
  oSQL := SQLOpen('YouTubeScraping',
    'SELECT Keywords FROM Customers where Id = ' + CustId + '');
  try
    sKeywords := oSQL.FieldByName('Keywords').AsString;
    arrayKeyword := StringReplace(sKeywords, ' ', '', [rfReplaceAll])
      .Split([',']);
    for I := 0 to Length(arrayKeyword) - 1 do
    begin
      lKeyword.Add(arrayKeyword[I]);
    end;
  finally
    oSQL.Free;
  end;
end;

end.
