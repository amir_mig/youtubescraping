unit uSearchChannels;

interface

uses
  Winapi.Windows,Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,Vcl.Graphics,
  Vcl.Controls,Vcl.Forms,Vcl.Dialogs,IPPeerClient,System.Rtti,
  System.Bindings.Outputs,Vcl.Bind.Editors,Data.Bind.EngExt,
  Vcl.Bind.DBEngExt,Vcl.StdCtrls,Vcl.ExtCtrls,Data.Bind.Components,
  System.JSON,uSQL,Data.DB,
  Data.Win.ADODB,
  REST.Client,
  REST.Types,
  Data.Bind.ObjectScope,
  Vcl.Grids,
  uMenu,
  Vcl.Imaging.pngimage, uModel,
  MSXml,
  System.NetEncoding,
  inifiles;

type
  TFormSearchChannels = class(TForm)
    Image11: TImage;
    Panel1: TPanel;
    Label1: TLabel;
    Button1: TButton;
    Button2: TButton;
    EditParamsquery: TEdit;
    StringGrid1: TStringGrid;
    RESTClient1: TRESTClient;
    RESTResponse1: TRESTResponse;
    BindingsList1: TBindingsList;
    REST_Youtube_Request: TRESTRequest;
    RESTRequestVid: TRESTRequest;
    Label2: TLabel;
    CbCustomer: TComboBox;
    LblCustomer: TLabel;
    EditMaxResult: TEdit;
    LblMaxResult: TLabel;
    procedure Image11Click(Sender: TObject);
    function GetChannelIds(lChannelIds:TStringList ): integer;
    procedure SearchYoutube(Sender: TObject);
    function ParseJsonChannel(json : TJSONValue) : TChannel;
    procedure PopulateChannelGrid(channels : TChannels );
    function GetApiKey(): string;
    procedure Button1Click(Sender: TObject);
    function GetChannels(listId : TStringList): TChannels;
    function GetChannelInfo(id : string): TChannel;
    procedure Button2Click(Sender: TObject);
    procedure SaveChannel(channel : TChannel);
    function GetCustomerList(lCustomer : TStringList) : integer;
    procedure FormShow(Sender: TObject);
    procedure CbCustomerDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormSearchChannels: TFormSearchChannels;
  channelsP : TChannels;
  sCustomerID : string;


implementation

{$R *.dfm}

procedure TFormSearchChannels.Image11Click(Sender: TObject);
begin
  Self.Close;
end;

function TFormSearchChannels.GetChannelIds( lChannelIds:TStringList ): integer;
var
  //jObject: TJSONObject;
  jSnippet: TJSONObject;
  jID: TJSONObject;
  jItem: TJSONArray;
  jItemValue: TJSONValue;
  rec : Integer;
  sBatchId : string;
  video : TVideo;
  jValue : TJSONValue;
  jObject : TJSONObject;

begin
  RESTClient1.BaseURL := 'https://www.googleapis.com/youtube/v3';
  REST_Youtube_Request.Resource := 'search';
  REST_Youtube_Request.Params.AddItem('part','snippet');
  REST_Youtube_Request.Params.AddItem('q',EditParamsquery.Text);
  REST_Youtube_Request.Params.AddItem('maxResults',EditMaxResult.Text);
  REST_Youtube_Request.Params.AddItem('type','channel');
  REST_Youtube_Request.Params.AddItem('key',GetApiKey());

  REST_Youtube_Request.Execute;
    jValue := RESTResponse1.JSONValue;
    jObject :=  jValue as TJSONObject;
    rec :=0;
    jItem := jObject.GetValue('items') as TJSONArray;
    for jItemValue in jItem do
    begin
      jSnippet := jItemValue.GetValue<TJSONObject>('snippet');
      lChannelIds.Add(jSnippet.GetValue<string>('channelId'));
    end;
    result := lChannelIds.Count;
end;


procedure TFormSearchChannels.SearchYoutube(Sender: TObject);
var
  lChannelIds : TStringList;
begin

  lChannelIds := TStringList.Create;
  try
      GetChannelIds(lChannelIds);
      channelsP := GetChannels(lChannelIds);
      PopulateChannelGrid(channelsP);
    finally
    lChannelIds.Free;
 end;
end;

function TFormSearchChannels.ParseJsonChannel(json : TJSONValue) : TChannel;
var
  channel : TChannel;
  jSnippet: TJSONObject;
  jStats: TJSONObject;
begin
    //jID := json.GetValue<TJSONObject>('id');
    jSnippet := json.GetValue<TJSONObject>('snippet');
    jStats := json.GetValue<TJSONObject>('statistics');
    channel.ChannelId :=  json.GetValue<string>('id');
    channel.ChannelTitle :=  jSnippet.GetValue<string>('title') ;
    channel.Description :=  jSnippet.GetValue<string>('description');
    channel.JoinDate :=  jSnippet.GetValue<string>('publishedAt') ;
    channel.Views :=  jStats.GetValue<string>('viewCount');
    channel.Subscribers := jStats.GetValue<string>('subscriberCount');
    channel.CustomerId :=  sCustomerID;
    result := channel;
end;

procedure TFormSearchChannels.PopulateChannelGrid(channels : TChannels );
 var I : integer;
begin
  StringGrid1.Rows[0].CommaText:= 'No,Id,Name,Description,JoinDate,Views, Subscribers';
  StringGrid1.ColWidths[0] := 50;
  StringGrid1.ColWidths[1] := 200;
  StringGrid1.ColWidths[2] := 300;
  StringGrid1.ColWidths[3] := 800;
  StringGrid1.ColWidths[4] := 200;
  StringGrid1.ColWidths[5] := 200;
  StringGrid1.ColWidths[6] := 200;
  StringGrid1.RowCount := length(channels)+2 ;
//  sBatchId:=formatdatetime('yyyymmddhhmmss',Now);
  for I := 0 to length(channels)-1 do
  begin
    StringGrid1.Rows[I+1][0]:=  (I+1).ToString ;
    StringGrid1.Rows[I+1][1]:=  channels[I].ChannelId;
    StringGrid1.Rows[I+1][2]:=  channels[I].ChannelTitle ;
    StringGrid1.Rows[I+1][3]:=  channels[I].Description;
    StringGrid1.Rows[I+1][4]:=  channels[I].JoinDate;
    StringGrid1.Rows[I+1][5]:=   FloatToStrF(StrToInt64(channels[I].Views), ffNumber, 8, 0);
    StringGrid1.Rows[I+1][6]:=  FloatToStrF(StrToInt64(channels[I].Subscribers), ffNumber, 8, 0);
  end;
end;

procedure TFormSearchChannels.Button1Click(Sender: TObject);
begin
SearchYoutube(nil);
end;

procedure TFormSearchChannels.Button2Click(Sender: TObject);
var
I : integer;
begin
   for I := 0 to length(ChannelsP)-1 do SaveChannel(ChannelsP[I]);
   ShowMessage('Save channel is completed');
end;

procedure TFormSearchChannels.CbCustomerDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
 CbCustomer.Canvas.TextRect(Rect, Rect.Left, Rect.Top,CbCustomer.Items.Names[Index]);
 sCustomerID := CbCustomer.Items.Values[CbCustomer.Items.Names[Index]];
end;

procedure TFormSearchChannels.FormShow(Sender: TObject);
var
  lCustomer : TStringList;
begin
  sCustomerID := '0';
  lCustomer:=TStringList.Create;
  try
  CbCustomer.Style := csOwnerDrawFixed;
    GetCustomerList(lCustomer);
    CbCustomer.Items:=lCustomer;
  finally
    lCustomer.Free;
  end;

end;

function TFormSearchChannels.GetApiKey(): string;
var
  IniFile   : TIniFile;
  strKey : string;
begin
    IniFile:=TIniFile.Create(ExtractFilePath(ParamStr(0)) + 'YoutubeScraping.ini');
    try
      result:=IniFile.ReadString('YoutubeConfig','key','default');
    finally
      IniFile.Free;
    end;
end;

function TFormSearchChannels.GetChannels(listId : TStringList): TChannels;
var
  rec : Integer;
  sChannelId : string;
  channels : TChannels;
begin

  rec :=0;
   Setlength(channels,listId.Count);
  for sChannelId in listId do
  begin
      channels[rec] := GetChannelInfo(sChannelId);;
      inc(rec);
  end;
    result := channels;
end;

function TFormSearchChannels.GetChannelInfo(id : string): TChannel;
var
 channelInfo : TChannel;

 jValue : TJSONValue;
 jObject  : TJSONObject;
 jItem : TJSONArray ;


begin
  RESTClient1.BaseURL := 'https://www.googleapis.com/youtube/v3';
  RESTREquestVid.Resource := 'channels';
  RESTREquestVid.Params[1].name:='key';
  RESTREquestVid.Params[1].Value:=GetApiKey();
  RESTREquestVid.Params[2].Name := 'id';
  RESTREquestVid.Params[2].Value := id;
try
      RESTREquestVid.Execute;
      jValue := RESTResponse1.JSONValue;
      jObject := jValue as TJSONObject;
      jItem := jObject.GetValue('items') as TJSONArray;

      jValue := jItem.Items[0] ;
      channelInfo := ParseJsonChannel(jValue);
      result := channelInfo;
   except
      on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
   end;


end;

procedure TFormSearchChannels.SaveChannel(channel: TChannel);
var
  oSQL: TADOQuery;
  sQuery: string;
begin
  oSQL := SQLOpen('YoutubeScraping',
    'Select Channelid, ChannelTitle From Channels');

  try
    if oSQL.Eof then
      exit;
    sQuery := 'Insert Into Channels (ChannelTitle,ChannelID,ChannelAvatar,Details,Descriptions,Subscribers,Views, CustomerId) Values(';
    sQuery := sQuery + QuotedStr(channel.ChannelTitle) + ',';
    sQuery := sQuery + QuotedStr(channel.ChannelId) + ',';
    sQuery := sQuery + QuotedStr('avatar') + ',';
    sQuery := sQuery + QuotedStr('details') + ',';
    sQuery := sQuery + QuotedStr(channel.Description) + ',';
    sQuery := sQuery + QuotedStr(channel.Subscribers) + ',';
    sQuery := sQuery + QuotedStr(channel.Views) + ',';
    sQuery := sQuery + QuotedStr(channel.CustomerId) + ')';
    // sQuery := sQuery + QuotedStr(channel.JoinDate)+')' ;
    // oSQL:=TADOQuery.Create(nil);
    if channel.ChannelId <> '' then
      SQLExec('YoutubeScraping', sQuery);
  finally
    oSQL.Free
  end;
end;

function TFormSearchChannels.GetCustomerList(lCustomer : TStringList) : integer;
var
  oSQL : TADOQuery;

begin
  try
    oSQL:=SQLOpen('YoutubeScraping','Select Name, ID From Customers');
    oSQL.Recordset.MoveFirst;
    while not oSQL.Eof do
    begin
      lCustomer.AddPair(oSQL.Recordset.Fields[0].Value,oSQL.Recordset.Fields[1].Value);
      oSQL.Recordset.MoveNext;
      if oSQL.Recordset.Eof then Exit;
    end;
    result:=oSQL.RecordCount;
  finally
    oSQL.Free;
  end;

end;




end.
