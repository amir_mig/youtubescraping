object FormYoutubeConfig: TFormYoutubeConfig
  Left = 0
  Top = 0
  Caption = 'Configuration'
  ClientHeight = 201
  ClientWidth = 328
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 328
    Height = 201
    Align = alClient
    TabOrder = 0
    object LblName: TLabel
      Left = 24
      Top = 32
      Width = 24
      Height = 13
      Caption = 'Email'
    end
    object LblKey: TLabel
      Left = 24
      Top = 80
      Width = 18
      Height = 13
      Caption = 'Key'
    end
    object EditEmail: TEdit
      Left = 88
      Top = 29
      Width = 209
      Height = 21
      TabOrder = 0
    end
    object BtnSaveConfig: TButton
      Left = 208
      Top = 152
      Width = 89
      Height = 25
      Caption = 'Save'
      TabOrder = 1
      OnClick = BtnSaveConfigClick
    end
    object MemoKey: TMemo
      Left = 88
      Top = 72
      Width = 209
      Height = 57
      Lines.Strings = (
        'MemoKey')
      TabOrder = 2
    end
  end
end
