unit uChannelHistory;

interface

uses
  Winapi.Windows,Winapi.Messages,System.SysUtils,System.Variants,System.Classes,Vcl.Graphics,
  Vcl.Controls,Vcl.Forms,Vcl.Dialogs,Vcl.ExtCtrls,Vcl.StdCtrls,
  Vcl.Imaging.pngimage,Vcl.Imaging.jpeg,System.Actions,Vcl.ActnList,
  Vcl.Styles,Vcl.Themes,Vcl.Touch.GestureMgr,Data.DB,Vcl.Grids,Vcl.DBGrids,uUtils,uSQL,Data.Win.ADODB,Vcl.DBCtrls;

type
  TChannelHistoryForm = class(TForm)
    Panel1 : TPanel;
    TitleLabel : TLabel;
    Image1 : TImage;
    ScrollBox1 : TScrollBox;
    AppBar : TPanel;
    GestureManager1 : TGestureManager;
    ActionList1 : TActionList;
    Action1 : TAction;
    PanelController : TPanel;
    ControllerGroupBox : TGroupBox;
    PanelDbGrid : TPanel;
    DBGridChannelCustomerList : TDBGrid;
    GroupBoxChannelList : TGroupBox;
    DBNavigator : TDBNavigator;
    LabelSearch : TLabel;
    EditSearch : TEdit;
    procedure BackToMainForm(Sender : TObject);

    procedure FormCreate(Sender : TObject);
    procedure Action1Execute(Sender : TObject);
    procedure FormResize(Sender : TObject);
    procedure FormKeyDown(Sender : TObject; var Key : Word; Shift : TShiftState);
    procedure FormGesture(Sender : TObject; const EventInfo : TGestureEventInfo;
      var Handled : Boolean);
    procedure FormShow(Sender : TObject);
    procedure GetChannelCustomerList();
    procedure EditSearchKeyPress(Sender : TObject; var Key : Char);
    procedure ChannelListTableWidth();

    private
        { Private declarations }
      procedure AppBarResize;
      procedure AppBarShow(mode : Integer);
    public
        { Public declarations }
  end;

var
  ChannelHistoryForm            : TChannelHistoryForm = nil;
  oSQL,oSQLSearch               : TADOQuery;
  oDataSource,oDataSourceSearch : TDataSource;
  bSearchChannel                : Boolean;

implementation

{$R *.dfm}


uses uMenu;

procedure TChannelHistoryForm.Action1Execute(Sender : TObject);
begin
  AppBarShow(-1);
end;

const
  AppBarHeight = 75;

procedure TChannelHistoryForm.AppBarResize;
begin
  AppBar.SetBounds(0,AppBar.Parent.Height - AppBarHeight,
    AppBar.Parent.Width,AppBarHeight);
end;

procedure TChannelHistoryForm.AppBarShow(mode : Integer);
begin
  if mode = -1 then // Toggle
      mode:=Integer(not AppBar.Visible);

  if mode = 0 then
      AppBar.Visible:=False
  else
  begin
    AppBar.Visible:=True;
    AppBar.BringToFront;
  end;
end;

procedure TChannelHistoryForm.FormCreate(Sender : TObject);
var
  LStyle                  : TCustomStyleServices;
  MemoColor,MemoFontColor : TColor;
begin
    // Set background color for memos to the color of the form, from the active style.
  LStyle:=TStyleManager.ActiveStyle;
  MemoColor:=LStyle.GetStyleColor(scGenericBackground);
  MemoFontColor:=LStyle.GetStyleFontColor(sfButtonTextNormal);

end;

procedure TChannelHistoryForm.FormGesture(Sender : TObject;
  const EventInfo : TGestureEventInfo; var Handled : Boolean);
begin
  AppBarShow(0);
end;

procedure TChannelHistoryForm.FormKeyDown(Sender : TObject; var Key : Word;
  Shift : TShiftState);
begin
  if Key = VK_ESCAPE then
      AppBarShow(-1)
  else
      AppBarShow(0);
end;

procedure TChannelHistoryForm.FormResize(Sender : TObject);
begin
  AppBarResize;
end;

procedure TChannelHistoryForm.FormShow(Sender : TObject);
begin
  bSearchChannel:=False;
  GetChannelCustomerList();
end;

procedure TChannelHistoryForm.BackToMainForm(Sender : TObject);
begin
  oSQL.Free;
  oDataSource.Free;

  if (bSearchChannel = True) then
  begin
    oSQLSearch.Free;
    oDataSourceSearch.Free;
  end;
  Hide;
  GridForm.BringToFront;
end;

procedure TChannelHistoryForm.EditSearchKeyPress(Sender : TObject; var Key : Char);
var
  sSearchWord : String;
begin
  if (Key=#13) then
  begin
    if (bSearchChannel = True) then
    begin
      oSQLSearch.Free;
      oDataSourceSearch.Free;
    end;

    sSearchWord:=EditSearch.Text;

    if sSearchWord.Contains('''') then
    begin
      sSearchWord:=StringReplace(sSearchWord,'''','''''',[rfReplaceAll])
    end;

    oSQLSearch:=SQLOpen('YouTubeScraping','Select Id,ChannelTitle,ChannelID,Subscribers,Views,Cast(Substring(Descriptions,1,100) as varchar(500)) Descriptions from Channels where ChannelTitle LIKE ''%'+sSearchWord+'%'''+
        ' or ChannelID LIKE ''%' + sSearchWord + '%''' +
        ' or Id LIKE ''%' + sSearchWord + '%''');

    oSQLSearch.Open;

    oDataSourceSearch:=TDataSource.Create(ChannelHistoryForm);
    try
      oDataSourceSearch.DataSet:=oSQLSearch;
      DBGridChannelCustomerList.DataSource:=oDataSourceSearch;
      DBNavigator.DataSource:=oDataSourceSearch;
      bSearchChannel:=True;
      if (EditSearch.Text = '') then
      begin
        DBGridChannelCustomerList.DataSource:=oDataSource;
        DBNavigator.DataSource:=oDataSource;
      end;
    except
      on E : Exception do
      begin
        oSQL.Free;
        oDataSource.Free;
      end;
    end;

    ChannelListTableWidth();
  end;

  if (Key=#08) then
  begin
    if (Length(EditSearch.Text)= 1) then
    begin
      DBGridChannelCustomerList.DataSource:=oDataSource;
      DBNavigator.DataSource:=oDataSource;
    end;
  end;
end;

procedure TChannelHistoryForm.GetChannelCustomerList();
begin
  oSQL:=SQLOpen('YouTubeScraping','Select Id,ChannelTitle,ChannelID,Subscribers,Views,Cast(Substring(Descriptions,1,100) as varchar(500)) Descriptions from Channels order by Id asc');
  oSQL.Open;

  oDataSource:=TDataSource.Create(ChannelHistoryForm);
  try
    oDataSource.DataSet:=oSQL;
    DBGridChannelCustomerList.DataSource:=oDataSource;
    DBNavigator.DataSource:=oDataSource;
  except
    on E : Exception do
    begin
      oSQL.Free;
      oDataSource.Free;
    end;
  end;

  ChannelListTableWidth();
end;

procedure TChannelHistoryForm.ChannelListTableWidth();
begin
  DBGridChannelCustomerList.Columns[0].Width:=50;
  DBGridChannelCustomerList.Columns[1].Width:=250;
  DBGridChannelCustomerList.Columns[2].Width:=250;
  DBGridChannelCustomerList.Columns[3].Width:=100;
  DBGridChannelCustomerList.Columns[4].Width:=150;
  DBGridChannelCustomerList.Columns[5].Width:=600;

end;

end.
