unit uModel;

interface
type

  TLanguage = record
    VideoId : string;
    Code : string;
    Reliable : string;
    Confidence : string;
   end;
   TLanguages = Array of TLanguage;

  TVideo = record
    VideoId : string;
    CategoryId : string;
    ChannelId : string;
    PostType : string;
    Keyword : string;
    Title : string;
    TitleLink : string;
    Description : string;
    Tags : string;
    ContentLink : string;
    PublishedDate : string;
    Duration : string;
    Likes : string;
    Dislikes : string;
    Views : string;
    Favorites : string;
    Comments : string;
    CustomerId : string;
    Languages : TLanguages;
    BatchId : string;

  end;
  TVideos = Array of TVideo;

  TChannel = record
    ChannelId : string;
    ChannelTitle : string;
    Description : string;
    ContentLink : string;
    JoinDate : string;
    Subscribers : string;
    Views : string;
    CustomerId : string;
    BatchId : string;
  end;

  TChannels = Array of TChannel;

  TComment = record

    CommentId : string;
    AuthorDisplayName : string ;
    AuthorProfileImageUrl : string ;
    AuthorChannelUrl : string;
    ChannelId: string;
    VideoId: string;
    TextDisplay: string;
    TextOriginal: string;
    ParentId: string;
    ViewerRating: string;
    LikeCount:string;
    ModerationStatus: string;
    PublishedAt: string;
    UpdatedAt: string;
  end;
  TComments = Array of TComment;




implementation

end.
