unit uApiServices;

interface

uses
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.StrUtils,
  System.Variants,
  System.Classes,
  System.IniFiles,
  System.JSON,
  System.NetEncoding,
  MSXml,
  REST.Client,
  REST.Types,
  uModel;

function ApiLanguage(sText: string; videoId: string): TLanguages;
function ApiYoutube(resource: string; params : TRESTRequestParameterList;  out jObject : TJSONObject): integer;
function GetApiKey() : string;
function SearchVideoList(query : string; sChannelId: string;  sPageToken: string; maxResult: string;  out jObject : TJsonObject): integer;


implementation

 function ApiLanguage(sText : string; videoId : string) : TLanguages;
var
  XMLHTTPRequest : IXMLHTTPRequest;
  oResponse      : TJSONValue;
  sResponse      : string;
  language       : TJSONValue;
  theLanguage    : string;
  theConfidence  : string;
  sReliable      : string;
  lLanguages     : TLanguages;
  jLanguages     : TJSONArray;
  I              : integer;
const
 sLanguageWS = 'https://ws.detectlanguage.com/0.2/detect?q=' ;
begin
  I:=0;
  XMLHTTPRequest:=CoXMLHTTP.Create;
  try
    XMLHTTPRequest.Open('GET',sLanguageWS +
    '"' + TNetEncoding.URL.Encode(sText) + '"',
      False,
      EmptyParam,
      EmptyParam);
    XMLHTTPRequest.setRequestHeader('Authorization','Bearer 36567e356b28eb56d43b62af6efe1697');
    XMLHTTPRequest.send('');
    sResponse:=XMLHTTPRequest.responseText;

    oResponse:=TJSONObject.ParseJSONValue(sResponse);
    jLanguages:=oResponse.GetValue<TJSONArray>('data.detections');

    if languages.Count > 0 then
    begin
      for language in jLanguages do
      begin
        SetLength(lLanguages,jLanguages.Count);
        lLanguages[I].videoId:=videoId;
        lLanguages[I].code:=language.GetValue<string>('language');
        lLanguages[I].Confidence:=language.GetValue<string>('confidence');
        if language.GetValue<string>('isReliable') = 'true' then lLanguages[I].reliable:='1'
        else lLanguages[I].reliable:='0';
        inc(I);
      end;

    end;
    result:=lLanguages;
  finally

  end;
end;

function ApiYoutube(resource: string; params : TRESTRequestParameterList;  out jObject : TJSONObject): integer;
var
  RESTRequest : TRESTRequest;
  RESTClient : TRESTCLient;
  RESTResponse : TRESTResponse;
  jValue : TJSONValue;
  jObjectTemp : TJSONObject ;

const
  sYoutubeBaseURL = 'https://www.googleapis.com/youtube/v3';
begin

  RESTClient := TRESTCLient.Create(nil);
  RESTRequest := TRESTRequest.Create(nil);
  RESTResponse := TRESTResponse.Create(nil);
  try
  RESTClient.BaseURL := sYoutubeBaseURL;
  RESTRequest.Resource := resource;
  RESTRequest.Client := RESTClient;
  RESTRequest.Response := RESTResponse;
  RESTRequest.Params := params;
  RESTRequest.Execute;

  jValue := RESTResponse.JSONValue;
  jObjectTemp := jValue.Clone as TJSONObject;
  jObject := jObjectTemp;
  finally
      RESTClient.Free;
      RESTRequest.Free;
      RESTResponse.Free;
  end;
end;

function SearchVideoList(query : string; sChannelId: string;  sPageToken: string; maxResult: string;  out jObject : TJsonObject): integer;
var
  params : TRESTRequestParameterList;
  jValue : TJSONValue;

begin
  params := TRESTRequestParameterList.Create(nil);

  try
  params.AddItem('part', 'snippet');
  params.AddItem('q', query);
  params.AddItem('maxResults', maxResult);
  params.AddItem('type', 'video');
  if sChannelId <> '' then
    params.AddItem('channelId', sChannelId);
  if sPageToken <> '' then
    params.AddItem('pageToken', sPageToken);
  params.AddItem('key', GetApiKey());
  ApiYoutube('search',params,jObject) ;
  finally
    params.Free;
  end;


end;

function GetApiKey() : string;
var
  IniFile : TIniFile;
  strKey  : string;
begin
  IniFile:=TIniFile.Create(ExtractFilePath(ParamStr(0)) + 'YoutubeScraping.ini');
  try
    result:=IniFile.ReadString('YoutubeConfig','key','default');
  finally
    IniFile.Free;
  end;
end;
end.
